namespace eval miniquick {

proc msgLogo {} {
	return "\00311,02 \002Mi\00315Ni\00311\00315Q\00311u\00315i\00311C\00315K\002 \003"
}
proc msgScoreMax {} {
	variable nScoreMax
	return "\00300,05 Punteggio da raggiungere:\002 $nScoreMax \002\003"
}
proc msgHelp {} {
	return "\00300,02 Trova una parola iniziando dalla prima riga in alto, puoi utilizzare una lettera per ogni riga. \003"
}
proc msgGameOver {} {
	return "\00311,02 \002Mi\00315Ni\00311\00315Q\00311u\00315i\00311C\00315K\002 \00300,05 terminato! \003"
}
proc msgURL {} {
	return "\00300,05 \002http://eggames.sourceforge.net\002 \003"
}
proc msgValues {} {
	return "\00311,2 Valori:\0032,11 a=1 b=4 c=5 d=4 e=1 f=4 g=5 h=7 i=1 j=8 k=8 l=3 m=3 n=3 o=1 p=4 q=7 r=2 s=2 t=2 u=4 v=5 w=8 x=8 y=8 z=6 \003"
}
proc msgGap {} {
	variable timeGap
	return "\0030,5 $timeGap secondi al prossimo round \003"
}
proc msgRound {round} {
	return "\0039,1 MiNi Quick ... round n°:\002 $round \002\003"
}
proc msgGridLine {letters} {
	return "\0038,2$letters \003"
}
proc msgGridLineVal {value} {
	return "0,2\002 valore \0034,2x $value \002\003"
}
proc msgGridLineMin {} {
	return " \0037,2\002 <-- lunghezza minima \002\003"
}
proc msgQueryBot {} {
	global botnick
	return "\0031,7 Scrivi la parola trovata in query a\002 $botnick \002\003"
}
proc msgCanRewrite {} {
	return "\00311,1 Per cambiarla puoi riscriverla. \003"
}
proc msgTimeAns {} {
	variable timeOut
	return "\00311,1 Avete\0038\002 $timeOut\002\00311,1 secondi da adesso. \003"
}
proc msgWarning {} {
	return "\00311,1 Mancano solo\0034\002 10 \002\00311,1secondi... Sbrigatevi! \003"
}
proc msgRoundEnd {round} {
	return "\00311,1 Fine\0039\002 $round\°\002\00311,1 round \003"
}
proc msgPlrEndTime {} {
	return "\0034\002 TEMPO SCADUTO! \002\003"
}
proc msgWinner {winner} {
	if {[llength [split $winner]] < 2} {set msg "Il vincitore è"} {set msg "I vincitori sono"}
	return "\00309,2 $msg\0038,2 $winner\00309! \003"
}
proc msgNoWords {} {
	return "\00300,5 Nessuna parola trovata ! \003"
}
proc msgBonusSpd {} {
	return "\(2 bonus VELOCITA\)"
}
proc msgBonusLen {} {
	return "\(4 bonus LUNGHEZZA\)"
}
proc msgPoints {} {
	return "punti \003"
}
proc msgPointsTot {points} {
	return " per un totale di\0034,2 $points\0038,2 punti \003"
}
proc msgResTitle {} {
	return "\00311,1 questi i Risultati : \003"
}
proc msgRes {player word score bonus} {
	return "\0034,2 $player\0038,2 con la parola\0034,2 $word\0038,2 ottiene\0034,2 $score\0038,2 $bonus"
}
proc msgScoreTitle {} {
	variable nRound
	return "\00309,2 CLASSIFICA dopo\00308 $nRound\00309 round... \003"
}
proc msgScore {pos player score} {
	return "\00309,2 $pos.\00304 $player\00308 con\00304 $score\00308 punti \003"
}
proc msgWordLenMin {len} {
	return "\0038,4 La lunghezza minima è $len lettere !\003"
}
proc msgWordInvalid {word} {
	return "\00315 La parola\0034\002 $word\002\00315 non è presente nella griglia. Riprova ! \003"
}
proc msgWordOk {word score} {
	return "\0039,2 Con la parola\0038,2 $word\0039,2 ottieni\0038,2 $score \0039,2punti. \003"
}
proc msgWordUnk {} {
	return "\0039,2 Parola non presente nel mio dizionario. \003"
}

}; # namespace miniquick
