# Keep file order
source config/modules/core.tcl
source config/modules/ctcp.tcl
source config/modules/channels.tcl
source config/modules/server.tcl
source config/modules/irc.tcl
#source config/modules/dns.tcl
#source config/modules/filesys.tcl
source config/modules/transfer.tcl
source config/modules/share.tcl
#source config/modules/compress.tcl
source config/modules/notes.tcl
source config/modules/console.tcl

loadmodule blowfish
#loadmodule seen
#loadmodule uptime
#loadmodule wire
#loadmodule woobie
