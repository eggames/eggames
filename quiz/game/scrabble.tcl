#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz]} {
	putlog "\/\!\\ [file tail [info script]]: quiz.tcl v1.0 or later required."
	return
}
namespace eval scrabble {

namespace import ::quiz::addTimer ::quiz::killTimers ::quiz::randMinMax \
                 ::quiz::getFilePathConf ::quiz::saveScore ::quiz::stop \
                 ::quiz::msg ::quiz::ntc ::quiz::tmsg ::quiz::tntc

proc onStart {what} {
	variable channel; variable debug; variable nScoreMax
	variable timeGap; variable timeOut; variable timeSubscr; variable timeWarn

	if {$debug} {
		set timeGap     1
		set nScoreMax  50
		set timeSubscr 20
	}
	tmsg [namespace current] [msgSubscr]

	addTimer [namespace current] [utimer 2 [list bind pubm $::quiz::cmdFlag \
		"$channel $::quiz::cmdPrefix$::quiz::cmdSubscr" \
		[namespace current]::onSubscribe]]

	addTimer [namespace current] [utimer [expr $timeSubscr + 3] \
		[namespace current]::subscribeEnd]
}
proc onStop {} {
	variable channel

	unbindAnswer

	catch {unbind pubm $::quiz::cmdFlag \
		"$channel $::quiz::cmdPrefix$::quiz::cmdLetter" \
		[namespace current]::onLetters}

	catch {unbind pubm $::quiz::cmdFlag \
		"$channel $::quiz::cmdPrefix$::quiz::cmdSubscr" \
		[namespace current]::onSubscribe}

	catch {unbind pubm $::quiz::cmdFlag \
		"$channel $::quiz::cmdPrefix$::quiz::cmdLeave" \
		[namespace current]::onUnsubscribe}
}
proc onSubscribe {nick mask hand chan text} {
	variable channel; variable arrScoreMatch; variable lstPlayers; variable arrLetters

	# Player already subscribed.
	if {[info exist arrScoreMatch($nick)]} {return}

	# Reset player's score and letter's array and add him to player list.
	set arrScoreMatch($nick) 0
	set arrLetters($nick) ""
	lappend lstPlayers $nick

	msg $channel [msgSubscribed $nick]
}
proc subscribeEnd {} {
	variable channel; variable arrScoreMatch; variable msgTimers; variable arrTimers
	variable timeMsg; variable timeGap

	unbind pubm $::quiz::cmdFlag "$channel $::quiz::cmdPrefix$::quiz::cmdSubscr" \
		[namespace current]::onSubscribe

	msg $channel [msgEndSubscr]

	set subscribed [array size arrScoreMatch]
	if {!$subscribed} {
		msg $channel [msgSubscrNone]
		stop [namespace current]
	} elseif {$subscribed == 1} {
		msg $channel [msgSubscrNoMin]
		stop [namespace current]
	} elseif {$subscribed > 1} {
		tmsg [namespace current] [msgSubscrWho]
		tmsg [namespace current] [msgPlayList]
		tmsg [namespace current] [msgWait10Secs]

		addTimer [namespace current] \
			[utimer $timeGap [namespace current]::matchStart]
    }
}
proc matchStart {} {
	variable channel;    variable timeMsg;    variable word
	variable lstPlayers; variable plrCurrent; variable arrLetters
	variable nRound

	set nRound 0
	set word [::quiz::dict::makeRndWord]
	set plrCurrent [lindex $lstPlayers 0]

	tmsg [namespace current] [msgStart]
	tmsg [namespace current] [msgScoreMax]
	tmsg [namespace current] [msgValuesAd]
	tmsg [namespace current] [msgValues]

	# In the first round we give 7 letters to each player.
	foreach player $lstPlayers {
		for {set i 0} {$i < 7} {incr i} {
			lappend arrLetters($player) [makeRndLetter]
		}
	}
	bind pubm $::quiz::cmdFlag "$channel $::quiz::cmdPrefix$::quiz::cmdLeave" \
		[namespace current]::onUnsubscribe

	bind pubm $::quiz::cmdFlag "$channel $::quiz::cmdPrefix$::quiz::cmdLetter" \
		[namespace current]::onLetters

	roundStart
}
proc roundStart {} {
	variable channel;    variable timeMsg;    variable word
	variable arrLetters; variable plrCurrent
	variable timeOut;    variable timeWarn;   variable arrTimers
	variable nRound

	incr nRound

	# Show current word
	tmsg [namespace current] [msgWord [colorize $word]]

	# Show coloured letters to current player
#	incr timeMsg -1
	tntc [namespace current] \
		[msgLetters [colorize $arrLetters($plrCurrent)]] $plrCurrent

	tmsg [namespace current] [msgStartRound]
	set timeMsg 1

	set tm [expr $timeOut + $timeMsg]
	addTimer [namespace current] [utimer $tm [namespace current]::remove]
	addTimer [namespace current] [utimer [expr $tm - $timeWarn] \
		[list ::quiz::msg $channel [msgWarning]]]

	bindAnswer
}
proc bindAnswer {} {
	variable channel

	bind pubm $::quiz::cmdFlag "$channel $::quiz::cmdPrefix$::quiz::cmdAnswer *" \
		[namespace current]::onAnswer

	bind pubm $::quiz::cmdFlag "$channel $::quiz::cmdPrefix$::quiz::cmdTurn" \
		[namespace current]::onTurnPass
}
proc unbindAnswer {} {
	variable channel

	catch {unbind pubm $::quiz::cmdFlag \
		"$channel $::quiz::cmdPrefix$::quiz::cmdAnswer *" \
		[namespace current]::onAnswer}

	catch {unbind pubm $::quiz::cmdFlag \
		"$channel $::quiz::cmdPrefix$::quiz::cmdTurn" \
		[namespace current]::onTurnPass}
}
proc onAnswer {nick mask hand chan text} {
	variable channel;    variable msgTimers;  variable timeMsg
	variable plrCurrent; variable arrLetters; variable word
	variable nScoreMax;  variable arrTimers;  variable lstPlayers
	variable nRound;     variable wordLenMin; variable arrScoreMatch

	if {$nick != $plrCurrent} {return}

	set text [lindex [split $text] 1]

	set length [string length $text]
	if {$length < $wordLenMin} {
		msg $channel [msgWordShort]
	} elseif {[::quiz::dict::check $text]} {
		set lstWord [split $text ""]
		set lstTemp $arrLetters($nick)
		set lstMiss ""
		set picked  0

		foreach letter $lstWord {
			if {[string match -nocase *$letter* $lstTemp]} {
				set idx     [lsearch -nocase $lstTemp $letter]
				set lstTemp [lreplace $lstTemp $idx $idx]
				continue
			} elseif {!$picked && [string match -nocase *$letter* $word]} {
				set picked 1
			} else {
				lappend lstMiss $letter
			}
		}
		if {[llength $lstMiss]} {
			msg $channel [msgWordMissed $lstMiss]
		} else {
			bindAnswer

			set arrLetters($nick) $lstTemp
			set lstBonus ""
			set points [getScoreWord $lstWord]

			tmsg [namespace current] [msgResult $lstWord $points]

			set lstWord [split $word ""]
			foreach letter $lstWord {
				if {[string match -nocase *$letter* $text]} {
					lappend lstBonus $letter
				}
			}
			if {[llength $lstBonus]} {
				set bonus [getScoreWord $lstBonus]
				set points [expr $points + $bonus]
				tmsg [namespace current] [msgBonus $lstBonus $bonus]
			}
			if {$length > 4} {
				set points [expr $points + $length]
				tmsg [namespace current] [msgBonusLen $length]
			}
			set arrScoreMatch($nick) [expr $arrScoreMatch($nick) + $points]
			tmsg [namespace current] [msgScoreRes $arrScoreMatch($nick)]
			if {$arrScoreMatch($nick) >= $nScoreMax} {
				saveScore [namespace current]
				stop [namespace current] 1
				return
			}
			killTimers arrTimers
			set word $text

			# After 1st round we give 1 letter to each player that still has some.
			# If a player remains with no letters, we give him 15.
			if {$nRound > [llength $lstPlayers]} {
				if {[llength $arrLetters($nick)]} {set n 1} {set n 15}
				for {set i 0} {$i < $n} {incr i} {
					lappend arrLetters($nick) [makeRndLetter]
				}
			}
			nextPlayer
			roundStart
		}
	} else {
		msg $channel [msgWordInvalid $text]
	}
}
proc onTurnPass {nick mask hand chan text} {
	variable arrLetters; variable arrTimers; variable plrCurrent

	if {$nick != $plrCurrent} {return}

	killTimers arrTimers
	lappend arrLetters($nick) [makeRndLetter]
	tmsg [namespace current] [msgTurnPass]

	unbindAnswer
	nextPlayer
	roundStart
}
proc removePlayer {player} {
	variable lstPlayers; variable arrLetters; variable plrCurrent

	set idx        [lsearch -nocase $lstPlayers $player]
	set lstPlayers [lreplace $lstPlayers $idx $idx]
	unset arrLetters($player)

	if {$debug} {putlog "removing $player idx $idx"}

	if {[llength $lstPlayers] == 1} {
		set plrCurrent [lindex $lstPlayers 0]
		putlog "current $plrCurrent wins"
		saveScore [namespace current]
		stop [namespace current] 1
		return
	}
	if {$player == $plrCurrent} {
		if {$debug} {putlog "current was $plrCurrent with idx $idx"}
		set idx [expr $idx - 1]
		if {$debug} {putlog "idx is now $idx"}
		if {$idx == -1} {
			set idx 0
			if {$debug} {putlog "idx: -1 -> $idx"}
		}
		set plrCurrent [lindex $lstPlayers $idx]
		nextPlayer
		if {$debug} {putlog "current is now $plrCurrent"}
		roundStart
	}
}
proc nextPlayer {} {
	variable lstPlayers; variable plrCurrent

	set idx [lsearch $lstPlayers $plrCurrent];    # Current player list index
	if {$idx == [expr [llength $lstPlayers] - 1]} {
		set plrCurrent [lindex $lstPlayers 0];    # Last index, switch to first
	} else {
		incr idx
		set plrCurrent [lindex $lstPlayers $idx]; # Move to the next index
	}
}
proc onUnsubscribe {nick mask hand chan text} {
	msg $chan [msgLeave]
	removePlayer $nick
}
proc onLetters {nick mask hand chan text} {
	variable lstPlayers; variable arrLetters

	if {[lsearch -nocase $lstPlayers $nick] < 0} {
		if {$debug} {putlog "$nick invalid player"}
		return
	}
	ntc $nick [msgLetters [colorize $arrLetters($nick)]]
}
proc remove {} {
	variable channel; variable plrCurrent; variable arrTimers; variable msgTimers
	variable timeMsg

	killTimers msgTimers
	killTimers arrTimers
	set timeMsg 1

	msg $channel [msgRemove]
	unbindAnswer
	removePlayer $plrCurrent
}
proc makeRndLetter {} {
	if {[rand 2]} {return [lindex {A E I O U} [rand 5]]}
	return [lindex {B C D F G H L M N P Q R S T V Z}  [rand 16]]
}
proc colorize {what} {
	set colWord "\002"
	set lstWord [split $what ""]
	foreach letter $lstWord {
		if {[string match -nocase {[aeilnorst]} $letter]} {
			append colWord " \00309$letter "
		} elseif {[string match -nocase {[dgmu]} $letter]} {
			append colWord " \00315$letter "
		} elseif {[string match -nocase {[bcp]} $letter]} {
			append colWord " \00313$letter "
		} elseif {[string match -nocase {[fhv]} $letter]} {
			append colWord " \00307$letter "
		} elseif {[string match -nocase {[jq]} $letter]} {
			append colWord " \00311$letter "
		} elseif {[string match -nocase {[kwxyz]} $letter]} {
			append colWord " \00304$letter "
		}
	}
	append colWord "\002"
	return [string toupper $colWord]
}
proc getScoreWord {lst} {
	set points 0
	foreach letter $lst {
		if {[string match -nocase {[aeilnorst]} $letter]} {
			set points [expr $points + 1]
		} elseif {[string match -nocase {[dgmu]} $letter]} {
			set points [expr $points + 2]
		} elseif {[string match -nocase {[bcp]} $letter]} {
			set points [expr $points + 3]
		} elseif {[string match -nocase {[fhv]} $letter]} {
			set points [expr $points + 4]
		} elseif {[string match -nocase {[jq]} $letter]} {
			set points [expr $points + 8]
		} elseif {[string match -nocase {[kwxyz]} $letter]} {
			set points [expr $points + 10]
		}
	}
	return $points
}
proc saveConfig {} {
	variable channel; variable nScoreMax;  variable debug; variable timeGap
	variable timeOut; variable timeSubscr; variable timeWarn
	set fileConf [getFilePathConf "scrabble"]
	set f [open $fileConf w]
	puts $f "namespace eval scrabble {"
	puts $f "	variable channel; variable nScoreMax;  variable debug; variable timeGap"
	puts $f "	variable timeOut; variable timeSubscr; variable timeWarn"
	puts $f ""
	puts $f "	set debug      $debug"
	puts $f "	set nScoreMax  $nScoreMax"
	puts $f "	set timeGap    $timeGap"
	puts $f "	set timeOut    $timeOut"
	puts $f "	set timeWarn   $timeWarn"
	puts $f "	set timeSubscr $timeSubscr"
	puts $f "	set channel    \"$channel\""
	puts $f "}"
	close $f
}
proc reset {} {
	variable channel;    variable lstPlayers; variable plrCurrent
	variable word;       variable nRound;     variable timeGap
	variable timeMsg;    variable timeSubscr; variable arrScoreMatch
	variable gamesWon;   variable msgTimers;  variable timeWarn
	variable nScoreMax;  variable arrLetters; variable arrTimers
	variable wordLenMin; variable debug;      variable timeOut

	set channel $::quiz::channel
	set debug   $::quiz::debug

	set lstPlayers ""; set timeSubscr 50; set timeMsg   1; set nScoreMax 150
	set word       ""; set nRound      0; set timeGap   5; set timeWarn   20
	set plrCurrent ""; set wordLenMin  3; set timeOut 100

	array set gamesWon {}; array set msgTimers {}; array set arrLetters {}
	array set arrScoreMatch   {}; array set arrTimers {}

	array unset gamesWon  *; array unset msgTimers *; array unset arrLetters *
	array unset arrTimers *; array unset arrScoreMatch *
}
proc clean {} {
	variable channel;    variable lstPlayers; variable plrCurrent
	variable word;       variable nRound;     variable timeGap
	variable timeMsg;    variable timeSubscr; variable arrScoreMatch
	variable gamesWon;   variable msgTimers;  variable timeWarn
	variable nScoreMax;  variable arrLetters; variable arrTimers
	variable wordLenMin; variable debug;      variable timeOut


	unset channel;  unset lstPlayers; unset plrCurrent; unset word
	unset nRound;   unset timeGap;    unset timeMsg;    unset timeSubscr
	unset timeWarn; unset nScoreMax;  unset wordLenMin; unset debug
	unset timeOut;

	array unset gamesWon;  array unset msgTimers
	array unset arrTimers; array unset arrScoreMatch; array unset arrLetters
}
}; # namespace scrabble
