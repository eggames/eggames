namespace eval witts {

proc msgLogo {} {
	return "\00300,02 ¤¤¤\00307 \002W\00300itts\002 ¤¤¤ \003"
}
proc msgGameOver {} {
	return "\00300,02 ¤¤¤\00307 \002W\00300itts\002 ¤¤¤ terminato! \002\003"
}
proc msgURL {} {
	return "\00300,04\002 http://eggames.sourceforge.net \002\003"
}
proc msg1 {} {
	return "\00300,02 Lo scopo del gioco è quello di creare una frase originale partendo da una traccia \003"
}
proc msg2 {} {
	return "\00300,02 Scaduto il tempo seguirà una votazione fatta da voi \003"
}
proc msg3 {} {
	global botnick
	return "\00300,02 Per inviare la continuazione alla traccia scrivere in query a\00307\002 $botnick\002\00300 la continuazione \003"
}
proc msg4 {} {
	global botnick
	return "\00300,02 Per inviare il voto scrivete in query a\00307\002 $botnick\002\00300 il numero della frase \003"
}
proc msg5 {} {
	return "\00300,02 Se Inviate una continuazione\00307\002 SIETE OBBLIGATI A VOTARE\002\00300 altrimenti perderete i punti appena ricevuti \003"
}
proc msg6 {n} {
	return "\00300,02 Sono presenti\00307\002 $n\002\00300 tracce nel database \003"
}
proc msgPollStart {} {
	global botnick
	return "\00300,02 Ora votate, scrivete in query a\00307\002 $botnick\002\00300 il numero della frase \003"
}
proc msgPollTime {} {
	variable timePoll
	return "\00300,02 Avete\00307\002 $timePoll\002\00300 secondi a partire da ora!!! \003"
}
proc msgWarning {} {
	variable timeWarn
	return "\00300,02 Attenzione mancano solo\00307,02\002 $timeWarn\002\00300 secondi!!! \003"
}
proc msgPollEnd {} {
	return "\00300,05 Votazione terminata!!! \003"
}
proc msgRound {question} {
	variable nRound
	return "\00300,05 La\00307 $nRoundª\00300 traccia è:\00307 $question \003"
}
proc msgRoundPlay {} {
	global botnick
	return "\00300,02 Scrivete in query a\00307\002 $botnick\002\00300 la vostra continuazione \003"
}
proc msgRoundTime {} {
	variable timeOut
	return "\00300,02 Avete\00307\002 $timeOut\002\00300 secondi a partire da ora!!! \003"
}
proc msgRoundEnd {} {
	return "\00300,05 Tempo scaduto! \003"
}
proc msgWinner {winner} {
	variable arrScoreTotal; variable nRound
	return "\00300,02 ¤¤¤ Il vincitore dopo\00307\002 $nRound\002\00300 tracce è\00307\002 $winner\002\00300 con\00307\002 $arrScoreTotal($winner)\002\00300 punti ¤¤¤ \003"
}

}; # namespace witts
