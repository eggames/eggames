namespace eval nickserv {

	set isIdentified 0

	proc identify {} {
		global nick botnick
		# Are we using the registered nick?
		if {$botnick == $nick} {
			putlog "NickServ: Identifying..."
			putquick "PRIVMSG NickServ :IDENTIFY $::nickserv::password"
			return 0
		}
		# Who is using our nick?
		putlog "NickServ: WHOIS $nick"
		putquick "WHOIS $nick"
	}
	# NickServ asks to identify
	proc onIdentifyRequest {nick host hand text dest} {
		putlog "NickServ: Got IDENTIFY request."
		::nickserv::identify
	}
	proc onWhoisReply {origin num reply} {
		global nick botnick
		if {[scan $reply "%s %s %s %s" bot unick uident uhost] == 4} {
			set host "$uident@$uhost"
			if {$botnick != $nick} {
				set cmd "RECOVER";     # Some one else is using our nick
				if {$host == [getchanhost $nick]} {
					set cmd "GHOST";   # Ghost connection?
				} elseif {[finduser "*!$host"] == "services"} {
					set cmd "RELEASE"; # Ask NickServ to release our nick
				}
				putlog "NickServ: $cmd $nick"
				putquick "PRIVMSG NickServ :$cmd $nick $::nickserv::password"
			}
		}
	}
	proc onIdentified {nick host hand text dest} {
		putlog "NickServ: Identified."
		set ::nickserv::isIdentified 1
		if {$::chanserv::needs} {
			::chanserv::bindNeed
		}
	}
	bind notc S "*You are now identified." ::nickserv::onIdentified
	bind notc S "*risulti identificato."   ::nickserv::onIdentified
	bind notc S "*IDENTIFY*"               ::nickserv::onIdentifyRequest
	bind raw  - 311                        ::nickserv::onWhoisReply
}
namespace eval chanserv {

	proc onNeedInvite {channel type} {
		putlog "ChanServ: Requesting invite ($channel)"
		putquick "PRIVMSG ChanServ :invite $channel"
		putserv "JOIN $channel"
	}
	proc onNeedKey {channel type} {
		putlog "ChanServ: Requesting key ($channel)"
		putquick "PRIVMSG ChanServ :getkey $channel"
	}
	proc onNeedOp {channel type} {
		global botnick
		putlog "ChanServ: Requesting op ($channel)"
		putquick "PRIVMSG ChanServ :op $channel $botnick"
	}
	proc onNeedUnban {channel type} {
		putlog "ChanServ: Requesting unban ($channel)"
		putquick "PRIVMSG ChanServ :unban $channel me"
	}
	proc onReceiveKey {nick addr hand text dest} {
		set msg     [split [stripcodes bcru $text] " "]
		set channel [lindex $msg 1]
		set key     [lindex $msg 4]
		putlog "ChanServ: Received key, joining $channel"
		putquick "JOIN $channel $key"
	}
	proc bindNeed {} {
		bind need - "* invite"            ::chanserv::onNeedInvite
#		bind need - "% key"               ::chanserv::onNeedKey
		bind need - "* op"                ::chanserv::onNeedOp
		bind need - "* unban"             ::chanserv::onNeedUnban
		bind notc S "Channel % key is: *" ::chanserv::onReceiveKey
	}
}
source config/services.tcl

putlog "Script: [file tail [info script]] 1.0"
