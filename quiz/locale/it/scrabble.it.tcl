namespace eval scrabble {

proc msgLogo {} {
	return "\00300,04\002 SCRABBLE \002\003"
}
proc msgGameOver {} {
	return "\00300,04\002 SCRABBLE \00308,02 Terminato! \002\003"
}
proc msgURL {} {
	return "\00300,04\002 http://eggames.sourceforge.net \002\003"
}
proc msgSubscr {} {
	variable timeSubscr
	return "\00300,02 Avete\00308\002 $timeSubscr secondi\002\00300 per iscrivervi al gioco mediante il comando\00308\002 $::quiz::cmdPrefix$::quiz::cmdSubscr \002\003"
}
proc msgSubscribed {who} {
	return "\00308,02\002 $who\002\00300 >> iscritto al gioco << \003"
}
proc msgEndSubscr {} {
	return "\00308,02\002 Fine delle iscrizioni! \002\003"
}
proc msgSubscrNoMin {} {
	return "\00308,02 Numero minimo di giocatori non raggiunto. \003"
}
proc msgSubscrNone {} {
	return "\00308,02 Nessun giocatore iscritto. \003"
}
proc msgSubscrWho {} {
	return "\00300,02 Gli iscritti al gioco sono: \003"
}
proc msgPlayList {} {
	variable lstPlayers
	return "\00308,02\002 $lstPlayers \002\003"
}
proc msgWait10Secs {} {
	variable timeGap
	return "\00300,02 Tra\00304\002 $timeGap secondi\002\00300 comincera' il gioco \003"
}
proc msgStart {} {
	return "\00300,04\002 SCRABBLE \002\00300,02 >>\002\00308 il match ha inizio! \002\00300<< \003"
}
proc msgScoreMax {} {
	variable nScoreMax
	return "\00300,02 Si vince a >>\002\00304 $nScoreMax punti! \002\00300<< \003"
}
proc msgValuesAd {} {
	return "\00300,02 Le lettere sono colorate in base al loro valore, secondo il seguente schema: \003"
}
proc msgValues {} {
	return "\002\0031,09 1pt \003 \0031,15 2pts \003 \0031,13 3pts \003 \0031,07 4pts \003 \0031,11 8pts \003 \0030,04 10pts \003\002"
}
proc msgWord {what} {
	return "\00300,02 La parola e'$what\003"
}
proc msgWordInvalid {word} {
	return "\00300,02 Mi dispiace, nel mio dizionario non c'e' la parola\00304\002 $word\002 \003"
}
proc msgStartRound {} {
	variable plrCurrent
	return "\00300,02 E' il turno di\00308\002 $plrCurrent\002\00300: scrivi\00308\002 $::quiz::cmdPrefix$::quiz::cmdAnswer \[parola\]\002\00300 per giocare oppure\002\00308 $::quiz::cmdPrefix$::quiz::cmdTurn\002\00300 per pescare una lettera \003"
}
proc msgLetters {what} {
	return "\00300,02 Le tue lettere >>$what"
}
proc msgTurnPass {} {
	variable plrCurrent
	return "\00308,02 $plrCurrent pesca una lettera e passa il turno \003"
}
proc msgWordShort {} {
	variable wordLenMin
	return "\00308,02 Puoi giocare solo parole che abbiano almeno $wordLenMin lettere.. \003"
}
proc msgWordMissed {lstLetters} {
	set msg "\00308,02 "
	if {[llength $lstLetters] > 1} {
		append msg "Ti mancano le lettere"
	} else {
		append msg "Ti manca la lettera"
	}
	append msg "[colorize $lstLetters]\00308per giocare \003"
	return $msg
}
proc msgResult {what points} {
	variable plrCurrent
	return "\00308,02 $plrCurrent\00300 ha giocato la parola: [colorize $what]\00300 (Del valore di\00304\002 $points\002\00300 punti) \003"
}
proc msgBonusLen {len} {
	return "\00300,02 +\00308\002$len\002\00300 punti bonus (\00300Parola di \00308\002$len\002\00300 lettere = \00308\002$len\002\00300 punti) \003"
}
proc msgBonus {what points} {
	return "\00300,02 +\00308\002$points\002\00300 punti bonus, grazie alle lettere [colorize $what]\003"
}
proc msgScoreRes {points} {
	variable plrCurrent
	return "\00308,02 $plrCurrent\00300 ha\00308\002 $points\002\00300 punti \003"
}
proc msgWinner {winner} {
	return "\00300,04\002 SCRABBLE \002\00300,02 >>\00308\002 $winner Vince! \00301,08(^\00304_\00301^)\002\003"
}
proc msgScoreTitle {} {
	return "\00300,04\002 SCRABBLE \002\00300,02 >>\00308\002 CLASSIFICA \002\003"
}
proc msgScore {pos plr score} {
	variable gamesWon
	return "\00308,02 $pos. $plr\00300 con\00308 $score\00300 punti \003"
}
proc msgWarning {} {
	variable plrCurrent; variable timeWarn
	return "\00301,08\002 \/!\\ \00300,04 $plrCurrent hai $timeWarn secondi per rispondere o verrai eliminato! \00301,08 \/!\\ \002\003"
}
proc msgLeave {} {
	variable plrCurrent
	return "\00304,02\002 $plrCurrent\002\00300 abbandona la partita \003"
}
proc msgRemove {} {
	variable plrCurrent; variable timeOut
	return "\00301,08\002 \/!\\ \00304,02 $plrCurrent \00300e'\00304 ELIMINATO! \00300\002\(Time Out $timeOut sec\) \00301,08\002 \/!\\ \002\003"
}

}; # namespace scrabble
