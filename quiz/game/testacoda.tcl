#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz]} {
	putlog "\/\!\\ [file tail [info script]]: quiz.tcl v1.0 or later required."
	return
}
namespace eval testacoda {

namespace import ::quiz::addTimer ::quiz::isActive ::quiz::killTimers \
                 ::quiz::msg ::quiz::ntc ::quiz::tmsg \
                 ::quiz::saveScore ::quiz::getFilePathConf ::quiz::randMinMax

proc onStart {what} {
	variable timeGap

	tmsg [namespace current] [msgHelp]
	tmsg [namespace current] [msgHelp2]
	tmsg [namespace current] [msgHelp3]
	tmsg [namespace current] [msgSubscrAd]

	addTimer [utimer $timeGap [namespace current]::roundSubscribe]
}
proc onStop {} {
	variable channel

	catch {unbind pubm - "$channel *" [namespace current]::onAnswer}

	catch {unbind pubm - "$channel $::quiz::cmdPrefix$::quiz::cmdSubscr" \
		[namespace current]::onSubscribe}
}
proc onAnswer {nick mask hand chan text} {
	variable channel;   variable plrCurrent;  variable jolly; variable cmdJolly
	variable letters;   variable nWordLen;    variable word
	variable lstPlayed; variable nWordLenMin; variable tmrRound
	variable timeMsg;   variable msgTimers

	if {![isActive "testacoda"] || $nick != $plrCurrent} {return}

	set text [string tolower $text]
	if {$debug} {
		putlog "onAnswer, $plrCurrent -> $text - jolly: $jolly($nick)"
	}
	# Il giocatore gioca il jolly
	if {$jolly($nick) && $text == $cmdJolly} {
		unbind pubm - "$channel *" [namespace current]::onAnswer
		killutimer $tmrRound

		tmsg [namespace current] [msgPlayJolly]
		ntc $plrCurrent [[namespace current]::msgWordPossible]

		set jolly($nick) 0
		if {$debug} {putlog "onAnswer, $plrCurrent jolly: $jolly($nick)"}
		roundStart
		return
	}
	# Verifico la parola del giocatore
	if {[string match -nocase "$letters*" $text]} {
		if {$debug} {putlog "onAnswer - $letters in $text -> DictCheck"}
		if {[string length $text] >= $nWordLenMin && \
			[string length $text] <= $nWordLen && [DictCheck $text]==0} {
			# Controllo delle parole usate
			set count 0
			set isOk 1
			while {$count != [llength lstPlayed]} {
				if {$text == [lindex $lstPlayed $count]} {
					if {$debug} {putlog "onAnswer, already used: $text"}
					set isOk 0
					break
				}
				incr count
			}
			if {$isOk == 1} {
				# La parola è accettata
				if {$debug} {putlog "onAnswer - DictCheck OK"}
				unbind pubm - "$channel *" [namespace current]::onAnswer
				killutimer $tmrRound
				putquick "PRIVMSG $channel :[msgWordOk $text]"
				set word $text
				lappend lstPlayed $text
				if {$debug} {putlog "onAnswer, word: $word"}
				roundStart
				return
			}
		}
	}
	if {$debug} {putlog "onAnswer * End * \( $text \)"}
}
proc onSubscribe {nick mask hand chan text} {
	variable channel; variable jolly; variable lstPlayers

	if {[info exists jolly($nick)]} {return}

	lappend lstPlayers $nick

	set jolly($nick) 1
	if {$debug} {
		putlog "onSubscribe - Player $nick - Jolly $jolly($nick)"
	}
	msg $channel [[namespace current]::msgSubscribed $nick]
}
proc makeWord {} {
	variable word; variable lstPlayed

	if {![isActive "testacoda"]} {return}

	# Creo una parola casuale da max 5 lettere di lunghezza
	set word ""
	while {[string length $word] > 5} {
		set word [DictMakeRndWord]
	}
	lappend lstPlayed $word

	if {$debug} {putlog "makeWord: $word"}
}
proc makeFirstWord {} {
	variable nRound; variable nPlayed; variable timeMsg
	variable msgTimers

	if {![isActive "testacoda"]} {return}

	set  nRound 0
	incr nPlayed

	makeWord

	if {$debug} {putlog "makeFirstWord, played: $nPlayed"}
	tmsg [namespace current] [msgRoundOne]

	roundStart
}
proc makePossibleWord {} {
	global DictLangDir
	variable letters;  variable wordPossible; variable nWordLenMin
	variable nWordLen; variable lstPlayed;  variable word

	if {![isActive "testacoda"]} {return}
	set rndLen [makeRndWordLen]
	if {$rndLen > 1} {makeRndWordRange $rndLen}
	set nWordLen [string length $word]
	set tempW [string range $word \
		[expr $nWordLen - $rndLen] [expr $nWordLen - 1]]
	set letter [string index $tempW 0]
	set dict_path $DictLangDir
	append dict_path "/" $letter ".txt"
	set fDict [open $dict_path r]
	set isOk 0
	while {![eof $fDict]} {
		set nLine [gets $fDict]
		if {[string match -nocase "$tempW*" $nLine] && \
			[string length $nLine] >= $nWordLenMin && \
			[string length $nLine] <= $nWordLen} {
			set count 0
			while {$count != [llength lstPlayed]} {
				if {[string tolower $nLine] == \
					[lindex $lstPlayed $count]} {
					set isOk 0
					break
				}
				incr count
			}
			set isOk 1
		}
		if {$isOk} {
			set wordPossible $nLine
			set letters $tempW
			close $fDict
			if {$debug} {putlog "makePossibleWord done, possible: $wordPossible, letters: $letters"}
			return 0
		}
	}
	close $fDict

	makePossibleWord
}
proc makeRndWordLen {} {
	global DictLangDir
	variable word; variable nWordLen

	set nWordLen [string length $word]
	if {$nWordLen == 3} {
		set tempLen [randMinMax 1 4]
	} else {
		set tempLen [randMinMax 1 8]
	}
	if {$tempLen == 1} {makeRndWordRange 1 ; return 1}
	if {$tempLen == 2} {set rndLen 2}
	if {$tempLen == 3 || $tempLen == 4} {set rndLen 3}
	if {$tempLen >= 5} {set rndLen 4}

	set temp_let [string range $word \
		[expr $nWordLen - $rndLen] [expr $nWordLen - 1]]
	set letter   [string index $temp_let 0]
	set dict_path $DictLangDir
	append dict_path "/" $letter ".txt"

	set fDict [open $dict_path r]
	set isOk 0
	while {![eof $fDict]} {
		if {[string match -nocase "$temp_let*" [gets $fDict]]} {set isOk 1}
		if {$isOk} {
			close $fDict
			return $rndLen
		}
	}
	close $fDict
	makeRndWordLen
}
proc makeRndWordRange {length} {
	variable nWordLenMin

	if {$length==4} {
		set tempLen [randMinMax 2 6]
	} else {
		set tempLen [randMinMax 1 6]
	}
	# Minimum
	if {$tempLen == 1} {set nWordLenMin [randMinMax 3 5]}
	if {$tempLen == 2} {set nWordLenMin [randMinMax 4 5]}
	if {$tempLen == 3} {set nWordLenMin [randMinMax 4 5]}
	if {$tempLen >= 4} {set nWordLenMin 5}

	# Maximum
	set tempLen [randMinMax 1 9]
	if {$tempLen <= 3} {set nWordLen [randMinMax 5 10]}
	if {$tempLen == 4} {set nWordLen [randMinMax 5 11]}
	if {$tempLen == 5} {set nWordLen [randMinMax 8 13]}
	if {$tempLen >= 6} {set nWordLen [randMinMax 9 15]}
}
proc roundSubscribe {} {
	variable channel; variable timeMsg; variable timeSubscr
	variable msgTimers

	if {![isActive "testacoda"]} {return}

	tmsg [namespace current] [msgSubscr]
	set timeMsg 0

	bind pubm - "$channel $::quiz::cmdPrefix$::quiz::cmdSubscr" \
		[namespace current]::onSubscribe

	if {$debug} {
		set timeSubscr 15
		putlog "\0030,3 * Start testacoda Debug Log * \003"
	}
	addTimer [utimer [expr $timeSubscr + $timeMsg] \
		[namespace current]::roundSetup]
}
proc roundSetup {} {
	variable channel; variable jolly;  variable lstPlayers
	variable timeGap; variable timeMsg; variable msgTimers

	if {![isActive "testacoda"]} {return}

	unbind pubm - "$channel $::quiz::cmdPrefix$::quiz::cmdSubscr" \
		[namespace current]::onSubscribe

	tmsg [namespace current] [msgEndSubscr]
	if {[array size jolly] > 1} {
		tmsg [namespace current] [msgSubscrWho]
		tmsg [namespace current] [msgWait10Secs]
		set lstPlayers ""
		if {$debug} {set timeGap 1}
		addTimer [utimer $timeGap [namespace current]::makeFirstWord]
		return
	} elseif {[array size jolly] == 1} {
		tmsg [namespace current] [msgSubscrNoMin]
	} else {
		tmsg [namespace current] [msgSubscrNone]
	}
	stop [namespace current]
}
proc roundStart {} {
	variable channel;    variable cmdJolly;  variable jolly;   variable nRound
	variable plrCurrent; variable timeMsg; variable msgTimers

	if {![isActive "testacoda"]} {return}

	incr nRound

	makePossibleWord

	set playersNum [array size jolly]
	set playerNum  [randMinMax 1 $playersNum]
	set timePlay   [expr [randMinMax 10 15] + 12]

	if {$debug} {
		putlog "roundStart - Round ------------ : $nRound"
		putlog "roundStart - Giocatori --------- : $playersNum"
	}
	set count 1
	foreach plr [array names jolly] {
		if {$count == $playerNum} {set plrCurrent $plr}
		incr count
	}
	if {$debug} {putlog "roundStart - Giocatore corrente : $plrCurrent"}

	set msg [msgPlrTime2]; if {$jolly($plrCurrent)} {append msg [msgPlrTime3]}

	tmsg [namespace current] [msgPlrTurn]
	tmsg [namespace current] [msgWordPlay]
	tmsg [namespace current] [msgPlrTime $timePlay $msg]

	bind pubm - "$channel *" [namespace current]::onAnswer

	addTimer [utimer $timePlay [namespace current]::roundEnd]
}
proc roundEnd {} {
	variable channel; variable jolly;     variable nRound; variable plrCurrent
	variable timeMsg; variable msgTimers; variable wordPossible

	if {![isActive "testacoda"]} {return}

	unbind pubm - "$channel *" [namespace current]::onAnswer

	tmsg [namespace current] [msgTimeUp]
	tmsg [namespace current] [msgPlrOut]
	puthelp "NOTICE $plrCurrent :[msgWordPossible]"

	if {$debug} {putlog "roundEnd - Giocatori : [array size jolly] -> Unset jolly($plrCurrent)"}
	unset jolly($plrCurrent)
	if {$debug} {putlog "roundEnd - Giocatori : [array size jolly]"}

	if {[array size jolly]==1} {
		set winner [array names jolly]
		if {$debug} {putlog "roundEnd - Winner : $winner"}

		tmsg [namespace current] [msgWinner $winner]
		saveConfig
		saveScore [namespace current] $winner
#		TODO: showBonus
		stop [namespace current] 1
		return
	}
	roundStart
}
proc saveConfig {} {
	variable cmdJolly; variable nPlayed
	set fileConf [getFilePathConf "testacoda"]
	set f [open $fileConf w]
	puts $f "namespace eval testacoda {"
	puts $f "	variable channel; variable cmdJolly; variable debug"
	puts $f "	variable nPlayed; variable timeGap;  variable timeSubscr"
	puts $f ""
	puts $f "	set debug      $debug"
	puts $f "	set nPlayed    $nPlayed"
	puts $f "	set timeGap    $timeGap"
	puts $f "	set timeSubscr $timeSubscr"
	puts $f "	set channel    \"$channel\""
	puts $f "	set cmdJolly   \"$cmdJolly\""
	puts $f "}"
	close $f
}
proc reset {} {
	variable channel;    variable letters;      variable cmdJolly; variable jolly
	variable lstPlayers; variable possibleWord; variable plrCurrent
	variable word;       variable nRound;       variable nPlayed
	variable nWordLen;   variable nWordLenMin;  variable lstPlayed
	variable timeGap;    variable timeMsg;      variable timeSubscr
	variable gamesWon;   variable msgTimers;    variable debug

	set channel $::quiz::channel
	set debug   $::quiz::debug
	set cmdJolly "jol"

	set lstPlayers   ""; set possibleWord ""; set plrCurrent ""
	set word         ""; set nRound        0; set nPlayed    0; set nWordLen 0
	set nWordLenMin   0; set lstPlayed    ""; set timeGap    5; set timeMsg  0
	set timeSubscr   50

	array unset jolly *; array unset gamesWon *; array unset msgTimers *
}
proc clean {} {
	variable channel;    variable letters;      variable cmdJolly; variable jolly
	variable lstPlayers; variable possibleWord; variable plrCurrent
	variable word;       variable nRound;       variable nPlayed
	variable nWordLen;   variable nWordLenMin;  variable lstPlayed
	variable timeGap;    variable timeMsg;      variable timeSubscr
	variable gamesWon;   variable msgTimers;    variable debug

	unset channel;    unset debug;        unset letters;    unset cmdJolly
	unset lstPlayers; unset possibleWord; unset plrCurrent; unset timeGap
	unset word;       unset nRound;       unset nPlayed;    unset timeMsg
	unset nWordLen;   unset nWordLenMin;  unset lstPlayed;  unset timeSubscr

	array unset jolly; array unset gamesWon; array unset msgTimers
}
}; # namespace testacoda
