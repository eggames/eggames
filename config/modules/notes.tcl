loadmodule notes
set notefile                "${nick}.notes"
set max-notes               50
set note-life               60
set allow-fwd               0
set notify-users            1
set notify-onjoin           1
