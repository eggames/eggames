#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz::scarabeo]} {
	putlog "\/\!\\ [file tail [info script]]: scarabeo.tcl required."
	return
}
namespace eval scaraplus {

namespace import ::quiz::addTimer ::quiz::randMinMax ::quiz::isAnagram \
                 ::quiz::killTimers ::quiz::showScore ::quiz::msg ::quiz::tmsg

proc onStart {what} {
	variable nScoreMax

	setScoreMax $what

	tmsg [namespace current] [::quiz::scarabeo::msgScoreMax $nScoreMax]
	tmsg [namespace current] [::quiz::scarabeo::msgStart]

	addTimer [namespace current] \
		[utimer 4 [namespace current]::roundStart]
}
proc onStop {} {
	catch {unbind msgm - * [namespace current]::onAnswer}
}
proc onAnswer {nick host hand text} {
	variable arrScoreMatch; variable lstLetters; variable arrWords
	variable arrScoreRound; variable plrFastest; variable nWordLenMin
	variable channel

	if {[onchan $nick $channel]} {
		if {[string length $text] >= $nWordLenMin && [DictCheck $text] == 0} {
			set text [string trim $text]
			# Avoid duplicates
			if {[info exists arrWords($nick)]} {
				foreach guessed $arrWords($nick) {
					if {$guessed == $text} {return 1}
				}
			}
			set letters [join $lstLetters]
			if {![isAnagram $text $letters]} {return 1}
			if {![info exists arrScoreMatch($nick)]} {set arrScoreMatch($nick) 0}
			if {![info exists arrScoreRound($nick)]} {set arrScoreRound($nick) 0}
			set arrScoreRound($nick) [expr $arrScoreRound($nick) + \
				[::quiz::scarabeo::getScoreWord $text]]

			lappend arrWords($nick) $text
		}
	}
	return 1
}
proc setScoreMax {max} {
	variable nScoreMax

	if {[string is integer -strict $max]} {
		if {$max >= 1 && $max <= 1000} {
			set nScoreMax $max
		} elseif {$max < 1} {
			set nScoreMax 1
		} elseif {$max > 1000} {
			set nScoreMax 1000
		}
	}
}
proc makeLetters {} {
	variable lstLetters

	set lstLetters  ""
	set nVowels     [randMinMax 2 5]
	set nWordLenMin [randMinMax 3 5]
	set nCons       [expr 9 - $nVowels]
	set count       0
	while {$count < $nCons} {
		set nRand [rand 16]
		if {$nRand == 7 || $nRand == 8} {
			lappend lstLetters [lindex {J K W X Y} [rand 5]]
		} else {
			lappend lstLetters [lindex {B C D F G H L M N P Q R S T V Z} $nRand]
		}
		incr count
	}
	set count 0
	while {$count < $nVowels} {
		lappend lstLetters [lindex {A E I O U} [rand 5]]
		incr count
	}
}
proc roundStart {} {
	variable arrWords; variable arrScoreRound; variable lstLetters
	variable nRound;   variable nWordLenMin;   variable timeOut
	variable timeMsg;  variable channel

	makeLetters
	array unset arrWords *
	array unset arrScoreRound *
	incr nRound
	set  nWordLenMin [randMinMax 3 5]

	tmsg [namespace current] [msgRoundAd]
	tmsg [namespace current] [::quiz::scarabeo::msgWord $lstLetters]
	tmsg [namespace current] [msgWordLenMin]
	tmsg [namespace current] [msgQueryBot]
	tmsg [namespace current] [::quiz::scarabeo::msgShowTime $timeOut]

	addTimer [namespace current] [utimer [expr $timeOut + $timeMsg - 10] \
		[list msg $channel [::quiz::scarabeo::msgWarn10Secs]]]

	addTimer [namespace current] \
		[utimer [expr $timeOut + $timeMsg] [namespace current]::roundEnd]

	bind msgm - * [namespace current]::onAnswer
}
proc roundEnd {} {
	variable arrScoreMatch; variable arrWords;  variable lstLetters
	variable arrScoreRound; variable nRound;    variable nScoreMax
	variable arrTimers;     variable msgTimers; variable timeMsg

	unbind msgm - * [namespace current]::onAnswer

	killTimers arrTimers
	killTimers msgTimers
	set hasWinner 0
	set timeMsg   1

	foreach player [array names arrScoreRound] {
		::quiz::pmsg $player [::quiz::scarabeo::msgPlrEndTime]
		set arrScoreMatch($player) \
			[expr $arrScoreMatch($player) + $arrScoreRound($player)]

		if {$arrScoreMatch($player) >= $nScoreMax} {set hasWinner 1}
	}
	tmsg [namespace current] [::quiz::scarabeo::msgRoundEnd]
	tmsg [namespace current] [msgResTitle]

	if {[array size arrScoreRound]} {
		foreach player [array names arrScoreRound] {
			tmsg [namespace current] \
				[msgRes $player $arrWords($player) $arrScoreRound($player)]
		}
	} else {
		tmsg [namespace current] [msgNoWords]
	}
	showScore [namespace current] arrScoreMatch 1

	if {$hasWinner} {
		stop [namespace current] 1
		return
	}
	tmsg     [namespace current] [::quiz::scarabeo::msgStart]
	addTimer [namespace current] [utimer 5 [namespace current]::roundStart]
}
proc reset {} {
	variable arrTimers;   variable msgTimers; variable channel; variable debug
	variable lstLetters;  variable nRound;    variable nScoreMax
	variable nWordLenMin; variable timeMsg;   variable timeOut
	variable arrScoreMatch; variable arrScoreRound

	set channel $::quiz::channel
	set debug   $::quiz::debug

	set lstLetters ""; set nRound   0; set nScoreMax 600; set nWordLenMin 0
	set timeMsg     1; set timeOut 70

	array unset arrScoreMatch *; array unset arrScoreRound *
	array unset arrTimers *;     array unset msgTimers *
}
proc clean {} {
	variable arrTimers;   variable msgTimers; variable channel; variable debug
	variable lstLetters;  variable nRound;    variable nScoreMax
	variable nWordLenMin; variable timeMsg;   variable timeOut
	variable arrScoreMatch; variable arrScoreRound

	unset channel; unset lstLetters;  unset nRound;  unset nScoreMax
	unset debug;   unset nWordLenMin; unset timeMsg; unset timeOut

	array unset arrScoreMatch; array unset arrScoreRound
	array unset arrTimers;     array unset msgTimers
}
}; # namespace scaraplus
