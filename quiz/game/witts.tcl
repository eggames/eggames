#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz]} {
	putlog "\/\!\\ [file tail [info script]]: quiz.tcl v1.0 or later required."
	return
}
namespace eval witts {

namespace import ::quiz::addTimer ::quiz::getFilePathConf ::quiz::tmsg

proc onStart {what} {
	set fh    [open "$::quiz::dirData/witts.txt" r];
	set dbLen [llength [split [read -nonewline $fh] "\n"] ]
	close $fh

	tmsg [namespace current] [msg1]
	tmsg [namespace current] [msg2]
	tmsg [namespace current] [msg3]
	tmsg [namespace current] [msg4]
	tmsg [namespace current] [msg5]
	tmsg [namespace current] [msg6 $dbLen]

	roundStart
}
proc onStop {} {
	catch {unbind msgm - * [namespace current]::onAnswer}
}
proc onAnswer {nick host hand text} {

}
proc roundStart {} {
	variable channel; variable nRound
	variable timeMsg; variable timeOut; variable timeWarn

	set fh    [open "$::quiz::dirData/witts.txt" r];
	set lines [split [read -nonewline $fh] "\n"]
	close $fh
	set dbLen [llength $lines]
	set nLine [rand $dbLen]
	set track [lindex $lines $nLine]
	incr nRound
	tmsg [namespace current] [msgRound $track]
	tmsg [namespace current] [msgRoundPlay]
	tmsg [namespace current] [msgRoundTime]

	addTimer [namespace current] [utimer [expr $timeOut - $timeWarn] \
		[list ::quiz::msg $channel [[namespace current]::msgWarning]]]

	addTimer [namespace current] \
		[utimer $timeOut [namespace current]::roundEnd]
}
proc roundEnd {} {
	tmsg [namespace current] [msgRoundEnd]
}
proc saveConfig {} {
	variable channel; variable debug
	variable timeOut; variable timePoll; variable timeWarn

	set fileConf [getFilePathConf "witts"]
	set f [open $fileConf w]
	puts $f "namespace eval witts {"
	puts $f "	variable channel; variable debug"
	puts $f "	variable timeOut; variable timePoll; variable timeWarn"
	puts $f ""
	puts $f "	set channel  \"$channel\""
	puts $f "	set debug    $debug"
	puts $f "	set timeOut  $timeOut"
	puts $f "	set timePoll $timePoll"
	puts $f "	set timeWarn $timeWarn"
	puts $f "}"
	close $f
}
proc reset {} {
	variable arrTimers; variable msgTimers; variable channel; variable debug
	variable nRound;    variable timeMsg;   variable timeOut; variable timePoll
	variable timeWarn

	set channel $::quiz::channel
	set debug   $::quiz::debug

	set nRound 0
	set timeMsg 1; set timeOut 60; set timePoll 40; set timeWarn 10

	array unset arrTimers *; array unset msgTimers *
}
proc clean {} {
	variable arrTimers; variable msgTimers; variable channel; variable debug
	variable nRound;    variable timeMsg;   variable timeOut; variable timePoll
	variable timeWarn

	unset channel; unset debug;   unset nRound
	unset timeMsg; unset timeOut; unset timePoll; unset timeWarn

	array unset arrTimers; array unset msgTimers
}
}; # namespace witts
