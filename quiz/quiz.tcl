#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
## \file quiz.tcl
# Contains common functions for all the games.

catch ::quiz::_onUninstall

## Main game script with common game functions.
namespace eval quiz {

namespace export addTimer getActive getChannel getFilePathConf getFilePathScore \
                 getLastMonthName getScriptDir isActive isAnagram isDirEmpty    \
                 killTimers normalize msg ntc tmsg tntc randMinMax saveScore    \
                 setActive setChannel setTarget showScore stop

## Returns the absolute path to the directory from where the script was
# loaded.
proc getScriptDir {} {
	set filePath [file normalize [info script]]
	set dirPath  [file dirname $filePath]
	return $dirPath
}
## Stops a game in a channel.
# @param ns     The game/namespace name.
# @param winner Winner nickname if any, optional.
proc stop {ns {winner ""}} {
	variable cmdFlag; variable cmdPrefix; variable cmdStop

	namespace upvar $ns timeMsg       timeMsg
	namespace upvar $ns arrScoreTotal arrScoreTotal
	namespace upvar $ns arrTimers     arrTimers

	regsub "::quiz::" $ns "" name
	set gameChan [getChannel $name]
	if {$gameChan == ""} {return}
	setActive $name 0

	unbind pubm $cmdFlag "$gameChan $cmdPrefix$cmdStop" \
		[namespace current]::_onStop

	killTimers arrTimers
	${ns}::onStop

	if {$winner != ""} {
		tmsg $ns [${ns}::msgWinner $winner]
		showScore $ns arrScoreTotal 1
		utimer $timeMsg [list ::quiz::_end $ns $gameChan]
		return
	}
	_end $ns $gameChan
}
## Returns whether a word is an anagram of the other
# @param word1 First  word
# @param word2 Second word
proc isAnagram {word1 word2} {

	if {[string trim $word1] == "" || [string trim $word2] == "" } {return 0}

	set lstLetters1 [split $word1 ""]
	set lstLetters2 [split $word2 ""]
	foreach letter $lstLetters1 {
		if {[string match -nocase *$letter* $lstLetters2]} {
			set idx [lsearch -nocase $lstLetters2 $letter]
			set lstLetters2 [lreplace $lstLetters2 $idx $idx]
		} else {
			return 0
		}
	}
	return 1
}
## Returns whether the game is active in the previously registered channel.
# @param name The game name.
proc isActive {name} {
	variable games

	if {![info exists games($name)]} {
		putlog "[msgNoGame $name]"
		return 0
	}
	return [lindex $games($name) 0]
}
## Checks if a given directory is empty.
# @param dir The directory to check.
proc isDirEmpty {dir} {
	# Get list of _all_ files in directory
	set filenames [glob -nocomplain -tails -directory $dir * .*]
	# Check whether list is empty (after filtering specials)
	expr {![llength [lsearch -all -not -regexp $filenames {^\.\.?$}]]}
}
## Sets if the game is running in the previously registered channel.
# @param name The game name.
# @param val  The game status (boolean).
# *           1 sets the active status to @true (default).
# *           0 sets the active status to @false, this deactivates the game.
proc setActive {name {val 1}} {
	variable games
	if {![info exists games($name)]} {
		putlog "[msgNoGame $name]"
		return
	}
	set games($name) "$val [lindex $games($name) 1]"
}
## Sets the channel name to the global list.
# @param name The game name.
# @param chan The channel where to play the game.
proc setChannel {name chan} {
	variable games

	if {![info exists games($name)]} {return}
	set games($name) "[lindex $games($name) 0] $chan"
}
## Returns the registered channel name for the given game.
# @param name The game name.
proc getChannel {name} {
	variable games

	if {![info exists games($name)]} {return ""}
	return [lindex $games($name) 1]
}
## Returns the active game name in a specified channel.
# @param chan The channel name.
proc getActive {chan} {
	variable games

	foreach name [array names games] {
		if {[lindex $games($name) 0] &&
			[lindex $games($name) 1] == $chan} {
			return $name
		}
	}
	return ""
}
## Sets the maximum game score.
# @param ns    The game/namespace name.
# @param score The new score value.
proc setTarget {ns score} {
	namespace upvar $ns nScoreMax nScoreMax

	if {[string is integer -strict $score]} {
		if {$score >= 1 && $score <= 1000} {
			set nScoreMax $score
		} elseif {$score < 1} {
			set nScoreMax 1
		} elseif {$score > 1000} {
			set nScoreMax 1000
		}
	}
}
## Returns the configuration file path for the given game name.
# @param name The game name.
proc getFilePathConf {name} {
	variable dirConf
	return "$dirConf/$name.conf.tcl"
}
## Returns the scores file path for the given game name.
# @param name The game name.
proc getFilePathScore {name} {
	variable dirScor
	return "$dirScor/$name.score"
}
## Saves the match scores.
# @param ns     The game name/namespace.
proc saveScore {ns} {
	namespace upvar $ns arrScoreTotal arrScoreTotal
	regsub "::quiz::" $ns "" name

	set fileScore [getFilePathScore $name]
	set f [open $fileScore w]
	foreach player [array names arrScoreTotal] {
		puts $f "$player $arrScoreTotal($player)"
	}
	close $f
}
## Show score when the round or game is over.
# @param ns   The game name/namespace.
# @param arr  The score's array.
# @param type Set this to 1 (@true) if you want to display results in a
#             decreasing order, 0 (default, @false) otherwise.
proc showScore {ns arr {type 0}} {
	namespace upvar $ns $arr scores

	if {![array size scores]} {return}
	set pos 1;
	set lst [list]

	foreach {key value} [array get scores] {lappend lst [list $key $value]}
	if {$type} {
		set lst [lsort -integer -index 1 -decreasing $lst]
		set title ${ns}::msgScoreTitle
		set score ${ns}::msgScore
	} else {
		set lst [lsort -integer -index 1 $lst]
		set title ${ns}::msgResTitle
		set score ${ns}::msgRes
	}
	tmsg $ns [$title]
	foreach entry $lst {
		tmsg $ns [$score $pos [lindex $entry 0] [lindex $entry 1]]
		if {$pos == 10} {break}
		incr pos
	}
}
## Add a given timer to the game's timers array.
# @param ns  The game name/namespace.
# @param tmr The timer to add.
proc addTimer {ns tmr} {
	namespace upvar $ns arrTimers arrTimers

	set arrTimers([array size arrTimers]) $tmr
}
## Kills all the timers in the given array.
# @param arr The timer array.
proc killTimers {arr} {
	upvar $arr tmrs

	foreach idx [array names tmrs] {
		catch {killutimer $tmrs($idx)}
	}
	array unset tmrs *
}
proc msg {who msg} {
	putnow "PRIVMSG $who :$msg"
}
proc ntc {who msg} {
	putnow "NOTICE $who :$msg"
}
## Sends channel messages every second using timers (anti flood).
# @param ns   The game name/namespace.
# @param msg  The message to show.
proc tmsg {ns msg} {_tmsg $ns $msg}

## Sends a notice to a player using timers (anti flood).
# @param ns   The game name/namespace.
# @param msg  The message to show.
# @param who  The destination, nickname or channel.
proc tntc {ns msg who} {_tmsg $ns $msg $who}

## Returns a random number included between the given range.
# @param min The minimum bound.
# @param max The maximum bound.
proc randMinMax {min max} { return [expr {$min +(int(rand()*($max - $min + 1)))}] }

## Suppresses accents and remove foreign letters from a given word.
proc normalize {word} {
	set word [_suppressAccents $word]
	set word [regsub -all {[^A-Za-z]} $word ""]
	return $word
}
## Returns a string to represents the previous month name.
# @param month The month number.
proc getLastMonthName {month} {
	switch $month {
		00 {return "dic"}
		01 {return "gen"}
		02 {return "feb"}
		03 {return "mar"}
		04 {return "apr"}
		05 {return "mag"}
		06 {return "giu"}
		07 {return "lug"}
		08 {return "ago"}
		09 {return "set"}
		10 {return "ott"}
		11 {return "nov"}
		default {return "---"}
	}
}
# Startup procedure.
proc _setup {} {
	set err 0
	if {[info tclversion] < 8.5} {
		putlog "\/\!\\ [file tail [info script]]: TCL 8.5 or later required."
		set err 1
	}
	if {[info exists numversion] && $numversion < 1062000} {
		putlog "\/\!\\ [file tail [info script]]: Eggdrop 1.6.20 or later required."
		set err 1
	}
	if {$err} {unset err; return} {unset err}

	_setDirectories
	variable cmdList; variable cmdPrefix; variable dirDict
	source $dirDict/dictionary.tcl

	_loadConfig "global"
	_loadLang   "global"

	array set games {};
	array unset games *;

	bind pub - $cmdPrefix$cmdList [namespace current]::_onRequestList

	setudef flag quiz

#	set flood-msg 50:60

	putlog "Script: [file tail [info script]] 1.0"
	_loadGames
}
# Global reset.
proc _reset {} {
	variable channel;   variable debug;     variable lang;      variable letters
	variable cmdFlag;   variable cmdHelp;   variable cmdHint;   variable cmdList
	variable cmdPrefix; variable cmdStop;   variable cmdSubscr; variable cmdTarget
	variable cmdValues; variable cmdAnswer; variable cmdLetter; variable cmdTurn
	variable cmdLeave

	set debug     0
	set channel   "#changeMe"
	set lang      "en"
	set letters   "abcdefghijklmnopqrstuvwxyz"
	set cmdFlag   "-|-"
	set cmdPrefix "!"
	set cmdAnswer "g"
	set cmdHelp   "help"
	set cmdHint   "hint"
	set cmdLeave  "exit"
	set cmdList   "games"
	set cmdLetter "l"
	set cmdStop   "stop"
	set cmdTurn   "p"
	set cmdSubscr "go"
	set cmdTarget "target"
	set cmdValues "values"
}
# Saves the global configuration.
proc _saveConfig {} {
	variable channel;   variable debug;     variable lang;      variable letters
	variable cmdFlag;   variable cmdHelp;   variable cmdHint;   variable cmdList
	variable cmdPrefix; variable cmdStop;   variable cmdSubscr; variable cmdTarget
	variable cmdValues; variable cmdAnswer; variable cmdLetter; variable cmdTurn
	variable cmdLeave

	set fileConf [getFilePathConf "global"]
	set f [open $fileConf w]
	puts $f "variable channel;   variable debug;     variable lang;      variable letters"
	puts $f "variable cmdFlag;   variable cmdHelp;   variable cmdHint;   variable cmdList"
	puts $f "variable cmdPrefix; variable cmdStop;   variable cmdSubscr; variable cmdTarget"
	puts $f "variable cmdValues; variable cmdAnswer; variable cmdLetter; variable cmdTurn"
	puts $f "variable cmdLeave"
	puts $f ""
	puts $f "set debug     $debug"
	puts $f "set channel   \"$channel\""
	puts $f "set lang      \"$lang\""
	puts $f "set letters   \"$letters\""
	puts $f "set cmdFlag   \"$cmdFlag\""
	puts $f "set cmdPrefix \"$cmdPrefix\""
	puts $f "set cmdAnswer \"$cmdAnswer\""
	puts $f "set cmdHelp   \"$cmdHelp\""
	puts $f "set cmdHint   \"$cmdHint\""
	puts $f "set cmdLeave  \"$cmdLeave\""
	puts $f "set cmdLetter \"$cmdLetter\""
	puts $f "set cmdList   \"$cmdList\""
	puts $f "set cmdStop   \"$cmdStop\""
	puts $f "set cmdTurn   \"$cmdTurn\""
	puts $f "set cmdSubscr \"$cmdSubscr\""
	puts $f "set cmdTarget \"$cmdTarget\""
	puts $f "set cmdValues \"$cmdValues\""
	close $f
}
# Saves the default english translation file.
# This can be useful only if something were goes wrong with the installation.
proc _saveLang {} {
	variable dirLang; variable lang

	set dirLng $dirLang/en
	if {![file exists $dirLng]} {
		file mkdir $dirLng
		set fileLang $dirLng/global.en.tcl
		set f [open $fileLang w]
		puts $f "proc msgLangNotFound {name fileLang} {"
		puts $f "	return \"\\(\\!\\) \$name: \$fileLang not found.\""
		puts $f "}"
		puts $f "proc msgNoGame {name} {"
		puts $f "	return \"\\(\\!\\) \$name game script not found.\""
		puts $f "}"
		puts $f "proc msgGameRunning {name} {"
		puts $f "	variable cmdStop"
		puts $f "	return \"\$name is already running, use \$cmdStop to stop the game.\""
		puts $f "}"
		puts $f "proc msgListNone {} {"
		puts $f "	return \"\\0038,2 No games enabled for this channel. \\003\""
		puts $f "}"
		puts $f "proc msgList {gameList} {"
		puts $f "	return \"\\0038,2\\002 Available games:\\0030 \$gameList \\002\\003\""
		puts $f "}"
		close $f
	}
}
# Sets and creates, if needed, all the necessary directories.
proc _setDirectories {} {
	variable dirMain; variable dirConf; variable dirComm; variable dirDict
	variable dirData; variable dirGame; variable dirHtml; variable dirLang
	variable dirScor; variable dirStat; variable dirTemp

	set dirMain [getScriptDir];    set dirDict "$dirMain/dictionary"
	set dirConf "$dirMain/config"; set dirComm "$dirMain/common"
	set dirData "$dirMain/data";   set dirGame "$dirMain/game";
	set dirHtml "$dirMain/html";   set dirLang "$dirMain/locale";
	set dirScor "$dirMain/score";  set dirStat "$dirMain/stat";
	set dirTemp "$dirMain/temp"; # trivia

	if {![file exists $dirConf]} {file mkdir $dirConf}
	if {![file exists $dirData]} {file mkdir $dirData}
	if {![file exists $dirGame]} {file mkdir $dirGame}
	if {![file exists $dirHtml]} {file mkdir $dirHtml}
	if {![file exists $dirLang]} {file mkdir $dirLang}
	if {![file exists $dirScor]} {file mkdir $dirScor}
	if {![file exists $dirStat]} {file mkdir $dirStat}
	if {![file exists $dirTemp]} {file mkdir $dirTemp}
}
# Loads the specified game configuration.
# Used for for both games and main script.
# @param name The game name or 'global' if the main script.
proc _loadConfig {name} {
	set fileConf [getFilePathConf $name]
	if {![file exists $fileConf]} {
		if {$name != "global"} {
			catch ${name}::saveConfig
			return 0
		} else {
			putlog "\(\i\) File $fileConf not found, saving defaults."
			_reset
			_saveConfig
			return 1
		}
	}
	source $fileConf
	return 1
}
# Loads the translation file for the current selected language.
# Used for for both games and main script.
# @param name The game name or 'global' if the main script.
proc _loadLang {name} {
	variable dirLang; variable lang

	set fileLang $dirLang/$lang/$name.$lang.tcl
	if {![file exists $fileLang]} {
		if {$name == "global"} {
			_saveLang
		} else {
			putlog "[msgLangNotFound $name $fileLang]"
			return 0
		}
	}
	source $fileLang
	return 1
}
# Loads the saved scores.
# @param name The game name.
proc _loadScore {name} {
	set fileScore [getFilePathScore $name]
	if ![file exists $fileScore] {return}

	array unset ${name}::arrScoreTotal *
	set f [open $fileScore r]
	while {[gets $f s] != -1} {
		set ${name}::arrScoreTotal([lindex $s 0]) [lindex $s 1]
	}
	close $f
}
# Loads all game scripts from the 'game' directory.
proc _loadGames {} {
	variable cmdFlag;   variable cmdPrefix; variable games; variable dirGame
	variable cmdTarget; variable cmdValues;

	foreach gameScript \
		[lsort [glob -nocomplain -type f -directory $dirGame *.tcl]] {
		if {![catch {source $gameScript}]} {
			set name [file tail [file rootname $gameScript]]
			if {![_loadLang $name]} {continue}
			catch ${name}::reset
			_loadConfig $name
			set chan [set ${name}::channel]
			set games($name) "0 $chan"

			bind pubm $cmdFlag "$chan $cmdPrefix$name*" \
				[namespace current]::_onStart

			bind pubm - "$chan $cmdPrefix$cmdTarget*" \
				[namespace current]::_onRequestTarget

			bind pubm - "$chan $cmdPrefix$cmdValues" \
				[namespace current]::_onRequestValues

			catch ${name}::clean
			putlog "Script: [file tail $gameScript]"
		} else {
			putlog "\/!\\ Error loading [file tail $gameScript]"
		}
	}
}
# Event triggered when someone has requested the game list
proc _onRequestList {nick mask hand chan text} {
	variable games

	if {![channel get $chan quiz]} {
		puthelp "PRIVMSG $chan :[msgListNone]"
		return
	}
	set gameList ""
	foreach game [array names games] {
		if {[getChannel $game] == $chan} {lappend gameList $game}
	}
	puthelp "PRIVMSG $chan :[msgList [lsort -dictionary $gameList]]"
}
# Event triggered when a player requests/sets the maximum game score.
proc _onRequestTarget {nick mask hand chan text} {
	set gameName [getActive  $chan]
	set gameChan [getChannel $gameName]
	if {![channel get $chan quiz] || $chan != $gameChan || ![isActive $gameName]} {
		return
	}
	setTarget $gameName [lindex $text 1]
	tmsg $gameName [msgTarget $gameName]
}
# Event triggered when a player requests the game's letter values.
proc _onRequestValues {nick mask hand chan text} {
	set gameName [getActive  $chan]
	set gameChan [getChannel $gameName]
	if {![channel get $chan quiz] || $chan != $gameChan || ![isActive $gameName]} {
		return
	}
	catch {tmsg $gameName [${gameName}::msgValues]}
}
# Event triggered when someone launched sone game.
proc _onStart {nick host hand chan text} {
	variable games; variable cmdFlag; variable cmdPrefix; variable cmdStop

	set name [string trim [string range $text \
		[string length $cmdPrefix] \
		[string length [lindex $text 0]]]]

	if {[info exists games($name)] && [channel get $chan quiz]} {
		if {[isActive $name]} {
			puthelp "NOTICE $nick :[msgGameRunning $name]"
			return
		}
		${name}::reset
		_loadConfig $name;
		set gameChan [getChannel $name]
		if {$chan != $gameChan} {
			${name}::clean
			return
		}
		setActive $name

		catch {_loadScore $name}
		bind pubm $cmdFlag "$gameChan $cmdPrefix$cmdStop" \
			[namespace current]::_onStop

		tmsg $name [${name}::msgLogo]

		${name}::onStart $text
	}
}
# Event triggered when someone has stopped the game.
proc _onStop {nick host hand chan text} {
	set active [getActive $chan]

	if {$active == ""} {return}
	stop $active
}
# Internal game over procedure.
proc _end {ns chan} {
	namespace upvar $ns msgTimers arrTimers
	namespace upvar $ns msgTimers msgTimers

	killTimers arrTimers
	killTimers msgTimers
	putquick "PRIVMSG $chan :[${ns}::msgGameOver]"
	putquick "PRIVMSG $chan :[${ns}::msgURL]"
	${ns}::clean
}
# normalize helper, suppress accents in letters.
proc _suppressAccents {word} {
	return [string map -nocase {
		"à" "a" "â" "a" "ä" "a" "ã" "a"
		"é" "e" "è" "e" "ê" "e" "ë" "e"
		"î" "i" "ï" "i" "ì" "i"
		"ô" "o" "ö" "o" "ò" "o" "õ" "o"
		"ù" "u" "û" "u" "ü" "u"
		"ç" "c" "ñ" "n"
	} $word]
}
# tmsg and tntc helper procedure.
proc _tmsg {ns msg {who ""}} {
	namespace upvar $ns msgTimers msgTimers
	namespace upvar $ns timeMsg   timeMsg
	namespace upvar $ns channel   channel

	if {$who != ""} {
		set msgType "NOTICE"
	} else {
		set msgType "PRIVMSG"
		set who $channel
	}
	set idx [array size msgTimers]
	set msgTimers($idx) [utimer $timeMsg [list putnow "$msgType $who :$msg"]]
	incr timeMsg
}
bind evnt - prerehash ::quiz::_onUninstall
# onUninstall procedure, triggered on bot rehash.
proc _onUninstall {args} {
	variable cmdFlag; variable cmdList; variable cmdPrefix; variable cmdStop
	variable games

	foreach name [array names games] {
		set chan [getChannel $name]

		catch {unbind pubm $cmdFlag "$chan $cmdPrefix$name*" \
			[namespace current]::_onStart}

		catch {unbind pubm $cmdFlag "$chan $cmdPrefix$cmdStop" \
			[namespace current]::_onStop}

		catch {unbind pubm - "$chan $cmdPrefix$cmdTarget*" \
			[namespace current]::_onRequestTarget}

		catch {unbind pubm - "$chan $cmdPrefix$cmdValues" \
			[namespace current]::_onRequestValues}

		catch ${name}::onStop
		catch {killTimers ${name}::msgTimers}
		catch {killTimers ${name}::arrTimers}
		namespace delete $name
	}
	catch {unbind pub - $cmdPrefix$cmdList [namespace current]::_onRequestList}

	unbind evnt - prerehash [namespace current]::_onUninstall

	namespace delete [namespace current]
}
}; # namespace quiz

namespace eval quiz { _setup }
