namespace eval dict {
	proc msg1 {} {return "La parola"}
	proc msg2 {} {return "e'"}
	proc msg3 {} {return "non e'"}
	proc msg4 {} {return "presente nel dizionario."}
	proc msg5 {} {return "stata aggiunta nel dizionario."}
	proc msg6 {} {return "stata rimossa dal dizionario."}
}
