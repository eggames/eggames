namespace eval testacoda {

proc msgLogo {} {
	return "\0039,1 ~~~~~\0038\002 T\002\00311esta\0038\002C\002\00311oda \0037TCL\0039 ~~~~~ \003"
}
proc msgGameOver {} {
	return "\0039,1 ~~~~~\0038\002 T\002\00311esta\0038\002C\002\00311oda \0037TCL\0039 Disattivato ~~~~~ \003"
}
proc msgURL {} {
	return "\0039,1 ~~~~~\0038 http://eggames.sourceforge.net\0039 ~~~~~ \003"
}
proc msgHelp {} {
	return "\0039,1 Gioco a turni che consiste nello scrivere una parola iniziante con la parte finale della parola precedentemente data \003"
}
proc msgHelp2 {} {
	set sylLen12 "\0039\(\0038\0022\002\0039\)"
	set sylLen3  "\0039\(\0038\0023\002\0039\)"
	return "\0039,1 Es. pip\0038po\0039 $sylLen12 --> \0038po\0039llo - pol\0038lo\0039 $sylLen12 --> \0038lo\0039ntra - lon\0038tra\0039 $sylLen3 --> \0038tra\0039ve \003"
}
proc msgHelp3 {} {
	return "\0039,1 L'ordine dei giocatori e' casuale e non verranno accettate le parole gia' utilizzate \003"
}
proc msgSubscrAd {} {
	variable timeGap
	return "\0038,2 Le iscrizioni inizieranno tra\002 $timeGap\002 secondi \003"
}
proc msgSubscr {} {
	variable timeSubscr
	set cmd "$::quiz::cmdPrefix$::quiz::cmdSubscr"
	return "\0038,2 Avete\0030,2\002 $timeSubscr\002\0038 secondi per iscrivervi al gioco mediante il comando\0030,2\002 $cmd \002\003"
}
proc msgPlayJolly {} {
	variable plrCurrent
	return "\00311,1\002 $plrCurrent\002\0038 gioca il Jolly e rimane in gara! \003"
}
proc msgWordPossible {} {
	variable wordPossible
	return "Una parola possibile era: $wordPossible"
}
proc msgWordOk {guess} {
	return "\0039,1 Parola accettata:\0037 $guess \003"
}
proc msgSubscribed {who} {
	return "\00311,1\002 $who\002\0039 Iscritto al gioco \003"
}
proc msgMancheOne {} {
	variable nPlayed; variable word
	return "\0039,1 Ha inizio il gioco!\002 $nPlayed\ª\002 partita. Si partirà dalla parola:\0037 $word \003"
}
proc msgEndSubscr {} {
	return "\0038,2 Fine delle iscrizioni! \003"
}
proc msgSubscrWho {} {
	variable lstPlayers
	return "\0039,1 Gli iscritti al gioco sono:\00311 $lstPlayers \003"
}
proc msgWait10Secs {} {
	return "\0038,2 Tra\002 10\002 secondi comincerà il gioco \003"
}
proc msgSubscrNoMin {} {
	return "\0038,2 Numero minimo di giocatori non raggiunto. \003"
}
proc msgSubscrNone {} {
	return "\0038,2 Nessun giocatore iscritto. \003"
}
proc msgPlrTurn {} {
	variable plrCurrent
	return "\0039,1 E' il turno di\00311\002 $plrCurrent \002\003"
}
proc msgWordPlay {} {
	variable letters; variable word; variable nWordLen; variable nWordLenMin
	return "\0039,1 La parola e'\0037 [string toupper $word]\0038\002 [string length $letters]\002\0039 \(\0038[string toupper $letters]\0039\) - Lunghezza Minima:\0038\002 $nWordLenMin\002\0039 - Lunghezza Massima:\0038\002 $nWordLen \003"
}
proc msgPlrTime {secs msg} {
	return "\0038,2 Hai $secs\002 $msg\003"
}
proc msgPlrTime2 {} {
	return "secondi per scrivere la parola. "
}
proc msgPlrTime3 {} {
	variable cmdJolly
	return "Per usare il Jolly:\0030,2\002 $cmdJolly \002"
}
proc msgTimeUp {} {
	return "\0038,2 Tempo scaduto! \003"
}
proc msgPlrOut {} {
	variable plrCurrent
	return "\00311,1 $plrCurrent\0039\002 sei eliminato! \002\003"
}
proc msgWinner {winner} {
	variable nManche
	return "\0039,1 Dopo\0038 $nManche manche\0039 il vincitore e'\00311\002 $winner\002\0039! \003"
}
proc msgScoreTitle {} {
	return "\0037,1 ©º°¨\0039 Hall of Fame\0037 ¨°º© \003"
}
proc msgScoreRes {pos who won} {
	variable gamesWon
	return "\0037,1 $pos\°:\0038 $who\0039 con\0038 $gamesWon($who)\0039 $won \003"
}
proc msgWinnings {num} {
	if {$num > 1} {return "vittorie"} {return "vittoria"}
}

}; # namespace testacoda
