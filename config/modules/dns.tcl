loadmodule dns
set dns-servers             "8.8.8.8 8.8.4.4"
set dns-cache               86400
set dns-negcache            600
set dns-maxsends            4
set dns-retrydelay          3
