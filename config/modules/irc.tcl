loadmodule irc
set bounce-bans             1
set bounce-exempts          0
set bounce-invites          0
set bounce-modes            0
set max-modes               30
set max-bans                20
set max-exempts             20
set max-invites             20
set use-exempts             0
set use-invites             0
set kick-fun                0
set ban-fun                 0
set learn-users             0
set wait-split              600
set wait-info               180
set mode-buf-length         200
set opchars                 "@"
# set opchars               "@&~"; TODO: check ircd
set no-chanrec-info         0

if {${net-type} == 1} {
    set prevent-mixing      1
} elseif {${net-type} == 5} {
    set kick-method         1
    set modes-per-line      3
    set include-lk          1
    set use-354             0
    set rfc-compliant       0
}
