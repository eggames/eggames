namespace eval wscrabble {

proc msgLogo {} {
	return "\00300,02 \002WScrabble - \0039T \0038C \0034L\002 \003"
}
proc msgGameOver {} {
	return "\00300,02 \002WScrabble - \0039T \0038C \0034L\00308 terminato! \002\003"
}
proc msgURL {} {
	return "\00300,04\002 http://eggames.sourceforge.net \002\003"
}
proc msgLetters {} {
	variable lstLetters
	return "\00300,02 \[\00308Lettere\00300\]\00300 - $lstLetters - \003"
}
proc msgWordsPossible {} {
	variable nWordsPossible; variable nWordsBest; variable nWordsBestLen
	return "\00300,02 \[\00315Ho trovato\00300 $nWordsPossible\00315 parole, di cui\00300 $nWordsBest\00315 di\002\00308 $nWordsBestLen\002\00315 lettere.\00300\] \003"
}
proc msgWarnEndRound {} {
	return "\00304,02 Attenzione, il tempo sta per terminare! \003"
}
proc msgRoundEnd {} {
	variable lstWordsBest
	return "\00300,02 \[\00315Manche terminata.\00300\]\00315 Parole più lunghe: \00308$lstWordsBest \003"
}
proc msgGuess {who what} {
	return "\00309,02 Bravo \00308$who\00309... \00309\[$what\] \00315accettata! Chi riuscirà a trovare una parola più \00308lunga\00315 di\00308 [string length $what]\00315 lettere? \003"
}
proc msgGuessBest {who what} {
	variable arrScoreTotal; variable nBonus
	return "\00309,02 Congratulazioni \00308$who\00309! \00315Non male \00309\[$what\]\00315 !! Hai guadagnato\00309 [string length $what] punti + $nBonus di bonus\00315! \(\00309\037$arrScoreTotal($who)\037\00315 punti\) \003"
}
proc msgRoundRes {} {
	variable arrScoreTotal; variable roundWinner; variable word
	return "\00308,02 $roundWinner\00315 Ha guadagnato\00309 [string length $word] punti\00315! \(\00309\037$arrScoreTotal($roundWinner)\037\00315 punti) \003"
}

}; # namespace wscrabble
