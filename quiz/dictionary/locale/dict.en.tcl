namespace eval dict {
	proc msg1 {} {return "The word"}
	proc msg2 {} {return "is"}
	proc msg3 {} {return "is not"}
	proc msg4 {} {return "in the dictionary."}
	proc msg5 {} {return "was added to the dictionary."}
	proc msg6 {} {return "was removed from the dictionary."}
}
