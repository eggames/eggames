namespace eval scarabeo {

proc msgLogo {} {
	return "\0037,02\002 SCARABEO\00308 Classic \002\003"
}
proc msgGameOver {} {
	return "\00307,02\002 SCARABEO\00308 Classic terminato! \002\003"
}
proc msgURL {} {
	return "\00300,04\002 http://eggames.sourceforge.net \002\003"
}
proc msgScoreMax {max} {
	return "\00300,02 Si vince a\00308\002 $max\002\00300 punti \003"
}
proc msgStart {} {
	return "\00300,02 Tra 5 secondi il prossimo round! \003"
}
proc msgWarn10Secs {} {
	return "\00300,02 Ancora\00308\002 10\002\00300 secondi. Presto! \003"
}
proc msgPlrEndTime {} {
	return "\0034\002 TEMPO SCADUTO! \002\003"
}
proc msgRoundEnd {} {
	return "\00300,02 Tempo scaduto! \003"
}
proc msgRoundAd {} {
	variable nRound
	return "\00307,02\002 SCARABEO\00308 Classic\00300 Round \00308,05 $nRound \002\003"
}
proc msgWord {letters} {
	return "\00300,02 Le lettere sono \00308,05\002 $letters \002\003"
}
proc msgQueryBot {} {
	global botnick
	return "\00300,02 Scrivi la parola in query a \00308,05\002 $botnick \002\003"
}
proc msgShowTime {timeOut} {
	return "\00300,02 Avete\00308\002 $timeOut\002\00300 secondi da questo istante \003"
}
proc msgScoreWord {what score} {
	return "\00300,02 Con la parola\0038 $what\00300 ottieni\00308 $score\00300 punti. \003"
}
proc msgWordBad {letters} {
	return "La lettere sono\002 $letters\002"
}
proc msgScoreTitle {} {
	return "\00307,02\002 SCARABEO \002\00300,02 >>\00308\002 CLASSIFICA \002\003"
}
proc msgScore {pos plr score} {
	return "\00308,02 $pos. $plr\00300 con\00308 $score\00300 punti \003"
}
proc msgBonusLen {} {
	return "\002\Lunghezza\[2\]\002 "
}
proc msgBonusSpd {} {
	return "\002\+ Velocità\[2\]\002 "
}
proc msgResTitle {} {
	variable lstLetters
	return "\00300,02 Le lettere erano\00308\002 $lstLetters \002\003"
}
proc msgRes {player word score msg} {
	return "\0030,2 $player\0038,2 con la parola\0030,2 $word\0038,2 guadagna\0030,2 $score\0038,2 punti\00311,2 $msg\003"
}
proc msgWinners {winners score} {
	if {[llength $winners] == 1} {
		return "Vince $lstWinners\002\0038,5 con\00311,5\002 $score\002\0038,5 punti"
	} else {
		return "$lstWinners\002\0038,5 vincono con\00311,5\002 $score\002\0038,5 punti"
	}
}

}; # namespace scarabeo
