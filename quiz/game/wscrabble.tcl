#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz]} {
	putlog "\/\!\\ [file tail [info script]]: quiz.tcl v1.0 or later required."
	return
}
namespace eval wscrabble {

namespace import ::quiz::addTimer ::quiz::getFilePathConf ::quiz::isAnagram \
                 ::quiz::killTimers ::quiz::msg ::quiz::normalize \
                 ::quiz::randMinMax ::quiz::saveScore ::quiz::stop ::quiz::tmsg

proc onStart {what} {

	roundStart
}
proc onStop {} {
	variable channel

	catch {unbind pubm $::quiz::cmdFlag "$channel *" \
		[namespace current]::onAnswer}
}
proc onAnswer {nick host hand chan text} {
	variable arrScoreTotal; variable channel;   variable lstLetters
	variable nBonus;        variable nGuessLen; variable nWordsBestLen
	variable roundWinner;   variable word

	if {[string length $text] <= $nGuessLen} {return 1}

	set word [string toupper [normalize [lindex $text 0]]]
	set strLetters [join $lstLetters]
	if {[isAnagram $word $strLetters]} {

		set wordLen [string length $word]
		set roundWinner $nick
		if {![info exists arrScoreTotal($nick)]} {
			set arrScoreTotal($nick) 0
		}
		if {$wordLen == $nWordsBestLen} {
			set arrScoreTotal($nick) \
				[expr $arrScoreTotal($nick) + $wordLen + $nBonus]

			msg $channel [msgGuessBest $nick $word]

			unbind pubm $::quiz::cmdFlag "$channel *" \
				[namespace current]::onAnswer

			resetTimers
			roundStart
		} else {
			set arrScoreTotal($nick) \
				[expr $arrScoreTotal($nick) + $wordLen]

			set nGuessLen $wordLen
			msg $channel [msgGuess $nick $word]
		}
		saveScore [namespace current]
	}
}
proc makeLetters {} {
	variable lstLetters; variable letters; variable nWordLenMax

	set lstLetters ""
	for { set i 0 } { $i < $nWordLenMax } { incr i } {
		set rand [expr int(rand() * [string length $letters])]
		set letter [string index $letters $rand]
		lappend lstLetters $letter
	}
}
proc makeAnswers {} {
	variable debug;          variable lstLetters; variable lstWordsBest
	variable nWordsPossible; variable nWordsBest; variable nWordsBestLen

	set nWordsBest     0
	set nWordsBestLen  0
	set nWordsPossible 0
	set strLetters     [join $lstLetters]
	set lstDict        ""
	set lstWordsBest   ""
	foreach dictFile [glob -nocomplain -type f -directory $::quiz::dict::dirLang *.txt] {
		set f [open $dictFile r]
		set lstDict [list {*}$lstDict {*}[split [read -nonewline $f] "\n"]]
		close $f
	}
	foreach w $lstDict {
		if {[isAnagram $w $strLetters]} {
			incr nWordsPossible
			set w [string toupper $w]
			if {[string length $w] > $nWordsBestLen} {
				set lstWordsBest ""
				lappend lstWordsBest $w
				set nWordsBest 1
				set nWordsBestLen [string length $w]
			} elseif {[string length $w] == $nWordsBestLen} {
				incr nWordsBest
				lappend lstWordsBest $w
			}
		}
	}
	if {$debug} {putlog $lstWordsBest}
}
proc roundStart {} {
	variable channel;   variable lstLetters; variable nGuessLen;  variable word
	variable nAutoStop; variable nRound;     variable roundWinner
	variable timeOut;   variable timeMsg;    variable timeWarn

	incr nRound
	if {$nAutoStop > 0 && $nRound >= $nAutoStop} {stop [namespace current] 1}

	set nGuessLen   0
	set word        ""
	set roundWinner ""
	makeLetters
	makeAnswers

	tmsg [namespace current] [msgLetters]
	tmsg [namespace current] [msgWordsPossible]

	addTimer [namespace current] [utimer [expr $timeOut + $timeMsg - $timeWarn] \
		[list ::quiz::msg $channel [[namespace current]::msgWarnEndRound]]]

	addTimer [namespace current] [utimer [expr $timeOut + $timeMsg] \
		[namespace current]::roundEnd]

	bind pubm $::quiz::cmdFlag "$channel *" [namespace current]::onAnswer
}
proc roundEnd {} {
	variable channel; variable timeMsg; variable roundWinner

	unbind pubm $::quiz::cmdFlag "$channel *" [namespace current]::onAnswer

	msg $channel [msgRoundEnd]

	if {$roundWinner != ""} {msg $channel [msgRoundRes]}

	resetTimers
	roundStart
}
proc resetTimers {} {
	variable arrTimers; variable msgTimers; variable timeMsg

	killTimers arrTimers
	killTimers msgTimers
	set timeMsg 1
}
proc saveConfig {} {
	variable channel; variable debug;   variable letters;  variable nAutoStop
	variable nBonus;  variable timeOut; variable timeWarn; variable nWordLenMax

	set fileConf [getFilePathConf "wscrabble"]
	set f [open $fileConf w]
	puts $f "namespace eval wscrabble {"
	puts $f "	variable channel; variable debug;   variable letters;  variable nAutoStop"
	puts $f "	variable nBonus;  variable timeOut; variable timeWarn; variable nWordLenMax"
	puts $f ""
	puts $f "	set debug       $debug"
	puts $f "	set nAutoStop   $nAutoStop"
	puts $f "	set nBonus      $nBonus"
	puts $f "	set nWordLenMax $nWordLenMax"
	puts $f "	set timeOut     $timeOut"
	puts $f "	set timeWarn    $timeWarn"
	puts $f "	set channel     \"$channel\""
	puts $f "	set letters     \"$letters\""
	puts $f "}"
	close $f
}
proc reset {} {
	variable arrScoreTotal;  variable arrTimers
	variable channel;        variable debug;         variable nGuessLen
	variable letters;        variable lstLetters;    variable lstWordsBest
	variable msgTimers;      variable nBonus;        variable nWordLenMax
	variable nWordLenMin;    variable nWordsBest;    variable nWordsBestLen
	variable nWordsPossible; variable timeMsg;       variable timeOut
	variable nAutoStop;      variable nRound;        variable timeWarn

	set channel $::quiz::channel
	set debug   $::quiz::debug
	set letters "AAAAAAAAABBCCDDDDEEEEEEEEEEEEFFGGGHHIIIIIIIIIJKLLLLMMNNNNNNOOOOOOOOPPQRRRRRRSSSSTTTTTTUUUUVVWWXYYZ"

	set lstLetters   ""; set lstWordsBest  ""; set nBonus    10; set nGuessLen 0
	set nWordLenMax  10; set nWordLenMin    0; set nWordsBest 0; set timeOut  70
	set nWordsBestLen 0; set nWordsPossible 0; set timeMsg    1; set timeWarn 30
	set nRound        0; set nAutoStop      0

	array unset arrScoreTotal *; array unset arrTimers *; array unset msgTimers *
}
proc clean {} {
	variable arrScoreTotal;  variable arrTimers
	variable channel;        variable debug;         variable nGuessLen
	variable letters;        variable lstLetters;    variable lstWordsBest
	variable msgTimers;      variable nBonus;        variable nWordLenMax
	variable nWordLenMin;    variable nWordsBest;    variable nWordsBestLen
	variable nWordsPossible; variable timeMsg;       variable timeOut
	variable nAutoStop;      variable nRound;        variable timeWarn

	unset channel;       unset debug;          unset letters
	unset lstLetters;    unset lstWordsBest;   unset nBonus;  unset nGuessLen
	unset nWordLenMax;   unset nWordLenMin;    unset nWordsBest
	unset nWordsBestLen; unset nWordsPossible; unset timeMsg; unset timeOut
	unset nAutoStop;     unset nRound;         unset timeWarn

	array unset arrScoreTotal; array unset arrTimers; array unset msgTimers
}
}; # namespace wscrabble
