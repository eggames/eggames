namespace eval dict {
	proc msg1 {} {return "Le mot"}
	proc msg2 {} {return "est"}
	proc msg3 {} {return "n'est pas"}
	proc msg4 {} {return "dans le dictionnaire."}
	proc msg5 {} {return "a été ajouté au dictionnaire."}
	proc msg6 {} {return "a été supprimée à partir du dictionnaire."}
}
