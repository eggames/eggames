set ::nickserv::password "nickserv_bot_password"
set ::chanserv::needs    0

namespace eval services {
    proc add {type} {
        if {![validuser services]} {
            adduser services *!service@azzurra.org
            setuser services HOSTS *!enforcer@azzurra.org
            setuser services HOSTS *!*@services.
            chattr  services -hp+fS
        }
    }
    bind evnt - userfile-loaded ::services::add
}
