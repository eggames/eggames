proc msgLangNotFound {name fileLang} {
	return "\(\!\) $name: $fileLang not found."
}
proc msgNoGame {name} {
	return "\(\!\) $name game script not found."
}
proc msgGameRunning {name} {
	variable cmdStop
	return "$name is already running, use $cmdStop to stop the game."
}
proc msgListNone {} {
	return "\0038,2 No games enabled for this channel. \003"
}
proc msgList {gameList} {
	if {$gameList == ""} {
		return "\0038,2 No games available for this channel. \003"
	}
	return "\0038,2\002 Available games:\0030 $gameList \002\003"
}
