namespace eval scaraplus {

proc msgLogo {} {
	return "\0037,02\002 SCARABEO\00309 \+\+PluS\+\+ \002\003"
}
proc msgURL {} {
	::quiz::scarabeo::msgURL
}
proc msgGameOver {} {
	return "\00307,02\002 SCARABEO\00309 \+\+PluS\+\+\00308 terminato! \002\003"
}
proc msgRoundAd {} {
	variable nRound
	return "\00307,02\002 SCARABEO\00309 \+\+PluS\+\+\00300 Round \00308,05 $nRound \002\003"
}
proc msgWordLenMin {} {
	variable nWordLenMin
	return "\00300,02 Il limite minimo è di\00308\002 $nWordLenMin\002\00300 lettere \003"
}
proc msgWordLenMax {} {
	variable nWordLenMax
	return "\00300,02 Il limite massimo è di\00308\002 $nWordLenMax\002\00300 lettere \003"
}
proc msgQueryBot {} {
	global botnick
	return "\00300,02 Scrivi le parole in query a \00308,05\002 $botnick \002\003"
}
proc msgResTitle {} {
	variable lstLetters
	return "\00300,02 Le lettere erano\00308\002 $lstLetters \002\003"
}
proc msgRes {player words score} {
	return "\00308,02 $player\00300 ha trovato\00308 $words\00300 e guadagna\00308 $score\00300 punti. \003"
}
proc msgScoreTitle {} {
	return "\00308,05\002 Classifica \002\003"
}
proc msgScore {pos player score} {
	return "\00300,02 $pos.\00308 $player\00300 con\00308 $score\00300 punti \003"
}
proc msgNoWords {} {
	return "\00300,5 Nessuna parola trovata ! \003"
}

}; # namespace scaraplus
