# KVIrc replies to avatar and userinfo CTCPs
#
# TCL by HelLViS69 & CtrlAltCa + minor changes (renamed and splitted files)
#
# Use KVIrc: get it on http://www.kvirc.net

source config/kvirc.tcl

set realname "\x03\x33\x0F${realname}"

bind ctcp - AVATAR        ctcp:avatar
bind ctcp - USERINFO      ctcp:userinfo
bind dcc m  chavatar      dcc:chavatar
bind bot b  change_avatar bot:change_avatar

proc ctcp:avatar {nick uhost hand dest key text} {
	global avatarz
	if {$avatarz!=""} {
		putserv "NOTICE $nick :\001AVATAR $avatarz\001"
		return 0
	}
}
proc ctcp:userinfo {nick uhost hand dest key test} {
	global userinfo
	if {$userinfo!=""} {
		putserv "NOTICE $nick :\001USERINFO $userinfo\001"
		return 0
	}
}

if {![file exists config/kvirc.avatar]} {
	#putlog "Creating Avatar.."
	set avatarz $kvircDefaultAvatar
	set fd [open config/kvirc.avatar w]
	puts $fd $avatarz
	close $fd
}
if {![info exists avatarz]} {
	#putlog "Reading Avatar.."
	set fd [open config/kvirc.avatar r]
	set avatarz [gets $fd]
	close $fd
}
proc dcc:chavatar { hand idx args } {
	global avatarz botnick
	set args [lindex $args 0]
	set avt [lindex $args 0]
	set tobot [lrange $args 1 end]
	if {($avt == "") || ($avt == " ")} { putdcc $idx "Usage: .chavatar <http://site.tld/avatar.png> \[botname\]" ; return 0 }
	if {($tobot == $botnick) || ($tobot == "") || ($tobot == " ")} {
		putlog "#$hand# change avatar.."
		set avatarz $avt
		set_avatar
		return 0
	}
	set botexist false
	foreach tempbot [bots] {
		if { [string tolower $tobot] == [string tolower $tempbot] } {set botexist true}
	}
	if {"$botexist" == "false"} {
		putdcc $idx "$tobot is not a bot or it is not linked..."
		return 0
	}
	putcmdlog "#$hand# change remote avatar on $tobot"
	putbot $tobot "change_avatar $hand ${avt}"
}
proc bot:change_avatar { bot cmd args } {
	global avatarz
	set hand [lindex [split $args] 0]
	set hand [string range $hand 1 end]
	set avt [lrange [split $args] 1 end]
	set lung [expr [string length $avt] - 3]
	set avatarz [string range $avt 0 $lung]
	putlog "#$hand@$bot# change Avatar.."
	set_avatar
}
proc set_avatar { } {
	global avatarz
	set fd [open config/kvirc.avatar w]
	puts $fd $avatarz
	close $fd
}
putlog "Script: [file tail [info script]] 2.0"
