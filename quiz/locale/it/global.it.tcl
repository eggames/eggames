proc msgNoGame {name} {
	return "\(\!\) $name: Gioco non trovato."
}
proc msgLangNotFound {name fileLang} {
	return "\(\!\) $name: $fileLang non trovato."
}
proc msgGameRunning {name} {
	variable cmdPrefix; variable cmdStop
	return "E' già in corso $name, usa $cmdPrefix$cmdStop per fermarlo."
}
proc msgListNone {} {
	return "\00308,2 Nessun gioco abilitato per questo canale \003"
}
proc msgList {gameList} {
	if {$gameList == ""} {
		return "\00308,2 Nessun gioco disponibile per questo canale \003"
	}
	return "\00308,2\002 I giochi disponibili sono:\00300 $gameList \002\003"
}
proc msgTarget {ns} {
	namespace upvar $ns nScoreMax target
	return "\00311,2 Target:\0032,11 $target \003"
}
