loadmodule filesys
set files-path              "~/eggdrop/filesys"
set incoming-path           "$files-path/incoming"
set upload-to-pwd           0
set filedb-path             ""
set max-file-users          20
set max-filesize            1024
