#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz]} {
	putlog "\/\!\\ [file tail [info script]]: quiz.tcl v1.0 or later required."
	return
}
namespace eval parolazza {

namespace import ::quiz::killTimers ::quiz::getFilePathConf ::quiz::msg \
                 ::quiz::saveScore ::quiz::stop

proc onStart {what} {
	variable channel;  variable first; variable last;    variable hintLen
	variable hintWord; variable word;  variable wordLen; variable debug

	set first   [makeWord "a"]
	set last    [makeWord "z"]
	set word    [::quiz::dict::makeRndWord]
	set wordLen [string length $word]
	set hintLen [expr $wordLen - 2]

	if {$debug} {putlog $word}

	set count 0
	while {$count < $wordLen} {
		append hintWord "-"
		incr count
	}
	msg $channel [msgRoundOne]

	bind pubm - * [namespace current]::onAnswer
	bind pubm - "$channel $::quiz::cmdPrefix$::quiz::cmdHint" \
		[namespace current]::onHintRequest
}
proc onStop {} {
	variable channel

	catch {unbind pubm - * [namespace current]::onAnswer}

	catch {unbind pubm - "$channel $::quiz::cmdPrefix$::quiz::cmdHint" \
		[namespace current]::onHintRequest}
}
proc onAnswer {nick mask hand chan text} {
	variable attempts; variable channel; variable first; variable last
	variable hintLen;  variable hasHint; variable hints; variable hintWord
	variable wordLen;  variable word;  variable arrScoreTotal

	if {$chan != $channel} {return}

	set text [string tolower $text]
	if {$text == $word} {
		set winner $nick
		incr arrScoreTotal($nick)
		incr attempts
		saveScore [namespace current]
		stop [namespace current] $winner
		return
	}
	if {[::quiz::dict::check $text]} {
		set inRange 0
		if {([string compare $text $first] > 0) && \
			([string compare $text $word] < 0)} {
			set first $text
			set inRange 1
		} elseif {([string compare $text $word] > 0) && \
				  ([string compare $text $last] < 0)} {
			set last $text
			set inRange 1
		}
		if {$inRange} {
			incr attempts
			if {$hints < $hintLen} {
				set lstHintWord [split $hintWord ""]
				set idx 0
				while {$idx < [expr $wordLen - 1]} {
					set letter [string index $last $idx]
					if {([string index $first $idx] == $letter) && \
						([lindex $lstHintWord $idx] == "-")} {
						set lstHintWord [lreplace $lstHintWord $idx $idx $letter]
						incr hints
						if {$hints == $hintLen} {break}
					} elseif {([string index $first $idx] == $letter) && \
							([lindex $lstHintWord $idx] != "-")} {
						incr idx
						continue
					} else {
						break
					}
					incr idx
				}
				set hintWord [string trim [join $lstHintWord ""]]
				if {$hints == $hintLen} {set hintWord "\0039$hintWord"}
			}
			if {!$hasHint} {
				msg $channel [msgRound]
			} else {
				msg $channel [msgHintWord]
			}
		}
	}
}
proc onHintRequest {nick mask hand chan text} {
	variable attempts; variable channel;  variable hints; variable hintLen
	variable hasHint;  variable hintWord; variable word;  variable wordLen

	if {$channel != $chan || $attempts == 0} {return}
	set hasHint 1
	if {$hints < $hintLen} {
		set count 0
		for {set idx $wordLen} {$idx > 1} {set idx [expr $wordLen - $count]} {
			set lstHintWord [split $hintWord ""]
			if {([lindex $lstHintWord $idx] == "-")} {
				set letter [string index $word $idx]
				set lstHintWord [lreplace $lstHintWord $idx $idx $letter]
				set hintWord [string trim [join $lstHintWord ""]]
				incr hints
				if {$hints == $hintLen} {set hintWord "\0039$hintWord"}
				break
			}
			incr count
		}
	}
	msg $channel [msgHintWord]
}
proc makeWord {letter} {
	variable word

	set dict_file_path $::quiz::dict::dirLang/$letter.txt
	if {![file exist $dict_file_path]} {
		putlog "\/\!\\ parolazza: [msgDictNotfound] $dict_file_path [msgLoadErr]"
		return
	}
	set dict_file [open $dict_file_path r]
	set dict_words [split [read $dict_file ] "\n"]
	switch $letter {
	"a" {
			set dict_sorted [lsort -dictionary $dict_words]
			set word [lindex $dict_sorted 2]
		}
	"z" {
			set dict_sorted [lsort -dictionary -decreasing $dict_words]
			set word [lindex $dict_sorted 0]
		}
	}
	close $dict_file
	return $word
}
proc saveConfig {} {
	variable channel; variable debug
	set fileConf [getFilePathConf "parolazza"]
	set f [open $fileConf w]
	puts $f "namespace eval parolazza {"
	puts $f "	variable channel; variable debug"
	puts $f ""
	puts $f "	set debug   $debug"
	puts $f "	set channel \"$channel\""
	puts $f "}"
	close $f
}
proc reset {} {
	variable attempts; variable channel; variable first; variable last
	variable hasHint;  variable hintLen; variable debug; variable hints
	variable hintWord; variable wordLen; variable word;  variable arrScoreTotal
	variable timeMsg

	set channel $::quiz::channel
	set debug   $::quiz::debug

	set first    ""; set attempts 0; set hasHint 0; set hints   0
	set last     ""; set hintLen  0; set wordLen 0; set timeMsg 1
	set hintWord ""; set word ""; array unset arrScoreTotal

}
proc clean {} {
	variable attempts; variable channel; variable first; variable last
	variable hasHint;  variable hintLen; variable debug; variable hints
	variable hintWord; variable wordLen; variable word;  variable arrScoreTotal
	variable timeMsg

	unset attempts; unset channel; unset first;    unset last; unset hasHint
	unset hintLen;  unset hints;   unset hintWord; unset word; unset wordLen
	unset debug;    unset timeMsg; array unset arrScoreTotal
}
}; # namespace parolazza
