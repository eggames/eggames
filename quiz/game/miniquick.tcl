#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz]} {
	putlog "\/\!\\ [file tail [info script]]: quiz.tcl v1.0 or later required."
	return
}
namespace eval miniquick {

namespace import ::quiz::addTimer   ::quiz::getFilePathConf ::quiz::isActive \
                 ::quiz::killTimers ::quiz::msg ::quiz::tmsg ::quiz::setTarget \
                 ::quiz::showScore  ::quiz::stop
proc onStart {what} {
	variable channel; variable debug; variable nScoreMax; variable timeOut
	variable timeGap

	setTarget [namespace current] [lindex $what 1]

	if {$debug} {set timeGap 1; set timeOut 50}

	tmsg [namespace current] [msgScoreMax]
	tmsg [namespace current] [msgHelp]

	roundSetup
}
proc onStop {} {
	variable channel

	catch {unbind msgm - * [namespace current]::onAnswer}
}
proc onAnswer {nick mask hand text} {
	variable channel;     variable grid;       variable nWordLen
	variable nWordLenMin; variable plrFastest; variable arrWords
	variable arrScoreRound;

	if {[onchan $nick $channel]} {

		set wordLen [string length $text]
		if {$nWordLenMin > $wordLen} {
			msg $nick [msgWordLenMin $nWordLenMin]
			return 1
		}
		set nLetters 1
		while {$nLetters < $wordLen + 1} {
			set letter [string index $text [expr $nLetters - 1]]
			if { ![string match -nocase "*$letter*" $grid($nLetters)] } {
				msg $nick [msgWordInvalid $text]
				return 1
			}
			incr nLetters
		}
		if {[::quiz::dict::check $text]} {
			set nLetters 1
			set score 0
			while {$nLetters < $wordLen + 1} {
				set letter [string index $text [expr $nLetters - 1]]
				if {      [string match -nocase "*$letter*" "aeio"]}  {set value 1
				} elseif {[string match -nocase "*$letter*" "rst"]}   {set value 2
				} elseif {[string match -nocase "*$letter*" "lmn"]}   {set value 3
				} elseif {[string match -nocase "*$letter*" "bdfpu"]} {set value 4
				} elseif {[string match -nocase "*$letter*" "cgv"]}   {set value 5
				} elseif { $letter == "z" }                           {set value 6
				} elseif {[string match -nocase "*$letter*" "hq"]}    {set value 7
				} elseif {[string match -nocase "*$letter*" "jkwxy"]} {set value 8
				} else                                                {set value 0}
				switch $nLetters {
					3 {set value [expr $value * 2]}
					4 {set value [expr $value * 3]}
					5 {set value [expr $value * 4]}
					6 {set value [expr $value * 5]}
					7 {set value [expr $value * 6]}
					8 {set value [expr $value * 7]}
				}
				set score [expr $score + $value]
				incr nLetters
			}
			if {$plrFastest == ""} {set plrFastest $nick}
			set arrWords($nick) $text
			set arrScoreRound($nick) $score
			if {$wordLen > $nWordLen} {set nWordLen $wordLen}
			msg $nick [msgWordOk $text $score]
			return 1
		}
		msg $nick [msgWordUnk]
		return 1
	}
}
proc makeGrid {} {
	variable grid; variable letters

	set lineNum 1
	while {$lineNum < 9} {
		set nLetters 1
		set sLetters ""
		while {$nLetters < 8} {
			set letter [string index $letters [rand 26]]
			append sLetters " $letter"
			incr nLetters
			if {$nLetters == 8} {
				if {![string match -nocase "*a*" $sLetters] && \
					![string match -nocase "*e*" $sLetters] && \
					![string match -nocase "*i*" $sLetters] && \
					![string match -nocase "*o*" $sLetters] && \
					![string match -nocase "*u*" $sLetters]} {
					set sLetters ""
					set nLetters 1
				}
			}
		}
		set grid($lineNum) $sLetters
		incr lineNum
	}
}
proc roundSetup {} {
	variable timeGap

	if {![isActive "miniquick"]} {return}

	makeGrid
	tmsg [namespace current] [msgGap]

	addTimer [namespace current] \
		[utimer $timeGap [namespace current]::roundStart]
}
proc roundStart {} {
	variable channel; variable nRound;   variable nWordLenMin
	variable timeOut; variable timeWarn; variable grid; variable timeMsg

	if {![isActive "miniquick"]} {return}

	incr nRound

	tmsg [namespace current] [msgRound $nRound]

	set nWordLenMin [expr [rand 2] + 3]
	set lineNum 1
	while {$lineNum < 9} {
		set sLetters  [string toupper $grid($lineNum)]
		set gridLine [msgGridLine $sLetters]
		set value    [expr $lineNum - 1]
		if {$lineNum > 2} {
			append gridLine [msgGridLineVal $value]
		}
		if {$nWordLenMin == $lineNum} {
			append gridLine [msgGridLineMin]
		}
		tmsg [namespace current] $gridLine
		incr lineNum
	}
	tmsg [namespace current] [msgQueryBot]
	tmsg [namespace current] [msgCanRewrite]
	tmsg [namespace current] [msgTimeAns]

	bind msgm - * [namespace current]::onAnswer

	addTimer [namespace current] \
		[utimer [expr $timeOut + $timeMsg - $timeWarn] \
		[list ::quiz::msg $channel [msgWarning]]]

	addTimer [namespace current] \
		[utimer [expr $timeOut + $timeMsg] [namespace current]::roundEnd]

	set timeMsg 1
}
proc roundEnd {} {
	variable hasWinner; variable nScoreMax;  variable arrWords
	variable nRound;    variable nWordLen;   variable arrScoreRound
	variable msgTimers; variable plrFastest; variable arrScoreMatch

	if {![isActive "miniquick"]} {return}

	unbind msgm - * [namespace current]::onAnswer

	if {[array size arrScoreRound] > 0} {
		foreach player [array names arrScoreRound] {
			msg $player [msgPlrEndTime]
		}
	}
	tmsg [namespace current] [msgRoundEnd $nRound]
	roundResult

	array unset arrWords *
	array unset msgTimers *
	array unset arrScoreRound *
	set plrFastest ""
	set nWordLen 0

	showScore [namespace current] arrScoreMatch 1

	if {[array size arrScoreMatch] > 0} {
		# How many winners? (same score)
		set winner ""; set score 0
		foreach player [array names arrScoreMatch] {
			if {$arrScoreMatch($player) >= $nScoreMax} {
				if {$winner == ""} {
					set winner $player
					set score $arrScoreMatch($player)
				} else {
					if {$player != $winner && $arrScoreMatch($player) > $score} {
						set winner $player
						set score $arrScoreMatch($player)
					}
					if {$player != $winner && $arrScoreMatch($player) == $score} {
						append winner " $player"
					}
				}
				set hasWinner 1
			}
		}
		# Show winner(s) if any
		if {$hasWinner} {
			stop [namespace current] $winner
			return
		}
	}
	roundSetup
}
proc roundResult {} {
	variable arrScoreRound; variable arrScoreMatch; variable plrFastest
	variable arrWords;      variable nWordLen

	if {![isActive "miniquick"]} {return}

	tmsg [namespace current] [msgResTitle]

	if {[array size arrScoreRound] <= 0} {
		tmsg [namespace current] [msgNoWords]
	} else {
		set lst [list]
		foreach {key value} [array get arrScoreRound] {
			lappend lst [list $key $value]
		}
		set lst [lsort -integer -index 1 $lst]
		foreach entry $lst {
			set bonus  0
			set msg    ""
			set player [lindex $entry 0]
			set word   $arrWords($player)
			set score  [lindex $entry 1]

			if {$player == $plrFastest} {
				set msg [append msg [msgBonusSpd]]
				set bonus 2
			}
			if {[string length $word] == $nWordLen} {
				set msg [append msg [msgBonusLen]]
				set bonus [expr $bonus + 4]
			}
			if {!$bonus} {
				set msg [msgPoints]
			} else {
				set tot [expr $score + $bonus]
				set msg [append msg [msgPointsTot $tot]]
			}
			tmsg [namespace current] \
				[msgRes $player $word $score $msg]

			if {[info exists arrScoreMatch($player)]} {
				set arrScoreMatch($player) \
					[expr $arrScoreMatch($player) + $score + $bonus]
			} else {
				set arrScoreMatch($player) [expr $score + $bonus]
			}
		}
	}
}
proc saveConfig {} {
	variable channel; variable debug;   variable nScoreMax
	variable timeGap; variable timeOut; variable timeWarn

	set fileConf [getFilePathConf "miniquick"]
	set f [open $fileConf w]
	puts $f "namespace eval miniquick {"
	puts $f "	variable channel; variable debug;   variable nScoreMax"
	puts $f "	variable timeGap; variable timeOut; variable timeWarn"
	puts $f ""
	puts $f "	set debug     $debug"
	puts $f "	set nScoreMax $nScoreMax"
	puts $f "	set timeGap   $timeGap"
	puts $f "	set timeOut   $timeOut"
	puts $f "	set timeWarn  $timeWarn"
	puts $f "	set channel   \"$channel\""
	puts $f "}"
	close $f
}
proc reset {} {
	variable letters;       variable plrFastest;    variable nRound
	variable nScoreMax;     variable nWordLen;      variable nWordLenMin
	variable timeMsg;       variable grid;          variable arrWords
	variable arrScoreRound; variable arrScoreMatch; variable msgTimers
	variable channel;       variable hasWinner;     variable debug
	variable timeGap;       variable timeOut;       variable timeWarn

	set channel     $::quiz::channel
	set debug       $::quiz::debug
	set letters     $::quiz::letters
	set plrFastest  ""
	set hasWinner   0
	set nRound      0
	set nScoreMax   200
	set nWordLen    0
	set nWordLenMin 0
	set timeMsg     1
	set timeGap     5
	set timeOut     80
	set timeWarn    10

	array unset arrScoreRound *; array unset arrWords *; array unset grid *
	array unset arrScoreMatch *; array unset msgTimers *
}
proc clean {} {
	variable letters;       variable plrFastest;    variable nRound
	variable nScoreMax;     variable nWordLen;      variable nWordLenMin
	variable timeMsg;       variable grid;          variable arrWords
	variable arrScoreRound; variable arrScoreMatch; variable msgTimers
	variable channel;       variable hasWinner;     variable debug
	variable timeGap;       variable timeOut;       variable timeWarn

	unset channel;     unset debug;      unset timeOut;   unset timeWarn
	unset letters;     unset nRound;     unset nScoreMax; unset nWordLen
	unset nWordLenMin; unset plrFastest; unset timeMsg;   unset hasWinner
	unset timeGap

	array unset arrScoreRound; array unset arrWords; array unset grid;
	array unset arrScoreMatch; array unset msgTimers
}
}; # namespace miniquick
