#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
if {![namespace exists ::quiz]} {
	putlog "\/\!\\ [file tail [info script]]: quiz.tcl v1.0 or later required."
	return
}
namespace eval scarabeo {

namespace import ::quiz::addTimer ::quiz::randMinMax ::quiz::stop \
                 ::quiz::killTimers ::quiz::msg ::quiz::tmsg

proc onStart {what {ns ""}} {

	if {$ns == ""} {set ns [namespace current]}
	namespace upvar ${ns} channel   channel
	namespace upvar ${ns} nScoreMax nScoreMax

	${ns}::setScoreMax $what

	tmsg $ns [msgScoreMax $nScoreMax]
	tmsg $ns [msgStart]

	addTimer $ns [utimer 4 ${ns}::roundStart]

	bind pubm - "$channel $::quiz::cmdPrefix$::quiz::cmdValues" \
		${ns}::onRequestValues
}
proc onStop {} {
	variable channel

	catch {unbind pubm - "$channel $::quiz::cmdPrefix$::quiz::cmdValues" \
		[namespace current]::onRequestValues}

	catch {unbind msgm - * [namespace current]::onAnswer}
}
proc onAnswer {nick host hand text} {
	variable arrRoundScore; variable arrWords;    variable isPlus
	variable lstLetters;    variable nWordLenMin; variable plrFastest

	if {[onchan $nick $channel]} {
		set text [string trim $text]
		if {[DictCheck $text] == 0} {
			set arrWords($nick) $text
			set arrRoundScore($nick) [getScoreWord $text]

			msg $nick [msgScoreWord $text $arrRoundScore($nick)]

			if {plrFastest == ""} {
				set plrFastest $nick
				set arrRoundScore($nick) [expr $arrRoundScore($nick) + 2]
			}
			return
		}
		msg $nick [msgWordBad $lstLetters]
	}
}
proc setScoreMax {max} {
	variable nScoreMax

	if {[string is integer -strict $max]} {
		if {$max >= 1 && $max <= 1000} {
			set nScoreMax $max
		} elseif {$max < 1} {
			set nScoreMax 1
		} elseif {$max > 1000} {
			set nScoreMax 1000
		}
	}
}
proc makeLetters {} {
	variable lstLetters

	set lstLetters ""
	set hasJolly   [rand 2]
	set nVowels    [randMinMax 3 6]
	set nCons      [expr 10 - $nVowels - $hasJolly]
	set count      0
	while {$count < $nCons} {
		set nRand [rand 16]
		if {$nRand == 7 || $nRand == 8} {
			lappend lstLetters [lindex {J K W X Y} [rand 5]]
		} else {
			lappend lstLetters [lindex {B C D F G H L M N P Q R S T V Z} $nRand]
		}
		incr count
	}
	if {$hasJolly} {lappend lstLetters *}
	set count 0
	while {$count < $nVowels} {
		lappend lstLetters [lindex {A E I O U} [rand 5]]
		incr count
	}
}
proc getScoreWord {what} {
	set points 0
	set lst [split $what ""]
	foreach letter $lst {
		if {[string match -nocase {[aceiorst]} $letter]} {
			set points [expr $points + 1]
		} elseif {[string match -nocase {[lmn]} $letter]} {
			set points [expr $points + 2]
		} elseif {[string match -nocase {[p]} $letter]} {
			set points [expr $points + 3]
		} elseif {[string match -nocase {[bdfguv]} $letter]} {
			set points [expr $points + 4]
		} elseif {[string match -nocase {[hz]} $letter]} {
			set points [expr $points + 8]
		} elseif {[string match -nocase {[q]} $letter]} {
			set points [expr $points + 10]
		} elseif {[string match -nocase {[jkxyw]} $letter]} {
			set points [expr $points + 12]
		}
	}
	return $points
}
proc roundStart {} {
	variable arrWords; variable channel; variable lstLetters; variable nRound
	variable timeOut;  variable arrRoundScore

	array unset arrWords *
	array unset arrRoundScore *
	makeLetters
	incr nRound

	tmsg [namespace current] [msgRoundAd]
	tmsg [namespace current] [msgWord $lstLetters]
	tmsg [namespace current] [msgQueryBot]
	tmsg [namespace current] [msgShowTime $timeOut]

	bind msgm - * [namespace current]::onAnswer

	addTimer [namespace current] [utimer [expr $timeOut - 10] \
		[list msg $channel [msgWarn10Secs]]]

	addTimer [namespace current] [utimer $timeOut [namespace current]::roundEnd]
}
proc roundEnd {} {
	variable arrRoundScore

	unbind msgm - * [namespace current]::onAnswer

	foreach player [array names arrRoundScore] {
		msg $player [msgPlrEndTime]
	}
	tmsg [namespace current] [msgRoundEnd]
	tmsg [namespace current] [msgRoundRes]

	if {[array size arrRoundScore] > 0} {
		foreach player [array names arrRoundScore] {
			set arrWordLen($player) [string length $arrWords($player)]
		}
		set lstWordLen [list]
		foreach {key value} [array get arrWordLen] {
			lappend lstWordLen [list $key $value]
		}
		set lstWordLen [lsort -integer -index 1 -decreasing $lst]
		set maxlen [lindex $lstWordLen 0]

		foreach player [array names arrRoundScore] {
			set arrBonus($player) ""
			if {$arrWordLen($player) == $maxlen} {
				set arrRoundScore($player) [expr $arrRoundScore($player) +2 ]
				append arrBonus($player) [msgBonusLen]
			}
			set arrMatchScore($player) \
				[expr $arrMatchScore($player) + $arrRoundScore($player)]

			if {$player == $plrFastest} {
				append arrBonus($player) [msgBonusSpd]
				set plrFastest ""
			}
		}
	}
	set lstWinners ""
	foreach player \
		[lsort -command ScaraSortMancheScores [array names arrRoundScore]] {
			tmsg [namespace current] [msgPointsRes $player $arrWords($player) \
				$arrRoundScore($player) $arrBonus($player)]

		set lstWinners [ScaraCheckWinner]
		unset arrRoundScore
	}
	ScaraClassifica $chan

	if {[llength $lstWinners] > 0} {
		tmsg [namespace current]
			[msgWinners $lstWinners $arrMatchScore([lindex $lstWinners 0])]

		stop [namespace current] 1
	}
	tmsg [namespace current] [msgStart]

	addTimer [namespace current] \
		[utimer 11 [namespace current]::roundSetup]
}
proc ScaraCheckWinner {} {
	variable nRound; variable arrMatchScore; variable nScoreMax

	set winner ""
	if {[array size arrMatchScore]} {
		set winscore 0
		foreach player [array names arrMatchScore] {
			if {$arrMatchScore($player) > $nScoreMax} {
				if {[llength $winner] == 0} {
					set winner $player
					set winscore $arrMatchScore($player)
				} else {
					if {$player != $winner && \
						$arrMatchScore($player) > $winscore} {
							set winner $player
							set winscore $arrMatchScore($player)
					}
					if {$player != $winner && \
						$arrMatchScore($player) == $winscore} {
							lappend winner $player
					}
				}
			}
		}
	}
	return $winner
}
proc reset {} {
	variable channel;    variable debug
	variable nRound;     variable nScoreMax;     variable plrFastest
	variable lstLetters; variable arrMatchScore; variable arrRoundScore
	variable timeMsg;    variable timeOut

	set debug   $::quiz::debug
	set channel $::quiz::channel

	set nRound      0; set plrFastest ""; set timeMsg 1
	set nScoreMax 200; set lstLetters ""; set timeOut 70

	array unset arrMatchScore *; array unset arrRoundScore *
}
proc clean {} {
	variable channel;    variable debug
	variable nRound;     variable nScoreMax;     variable plrFastest
	variable lstLetters; variable arrMatchScore; variable arrRoundScore
	variable timeMsg;    variable timeOut

	unset channel;   unset nRound;     unset nRound;     unset timeMsg
	unset nScoreMax; unset plrFastest; unset lstLetters; unset timeOut
	unset debug

	array unset arrMatchScore; array unset arrRoundScore
}
}; # namespace scarabeo
