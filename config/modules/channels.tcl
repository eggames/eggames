loadmodule channels
set chanfile                "${nick}.chan"
set force-expire            0
set share-greet             0
set use-info                1
set allow-ps                0

set global-flood-chan       0:0
set global-flood-deop       0:0
set global-flood-kick       0:0
set global-flood-join       0:0
set global-flood-ctcp       0:0
set global-flood-nick       0:0
set global-aop-delay        0:0
set global-chanmode         ""
set global-stopnethack-mode 0
set global-revenge-mode     0
set global-ban-time         0
set global-exempt-time      0
set global-invite-time      0
set global-idle-kick        0

set global-chanset {
    -autoop         -autovoice
    -autohalfop     -cycle
    -dontkickops    -dynamicbans
    -dynamicexempts -dynamicinvites
    -enforcebans    -greet
    -inactive       -nodesynch
    -protectfriends -protecthalfops
    -protectops
    -revenge        -revengebot
    -secret         -seen
    -shared         -statuslog
    +userbans       +userexempts
    +userinvites    -udef-flag-extrabitch
}
