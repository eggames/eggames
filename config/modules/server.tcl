loadmodule server
set keep-nick               1
bind evnt - init-server evnt:init_server
proc evnt:init_server {type} {
    global botnick
    putquick "MODE $botnick +i-ws"
}
# set init-server           { putserv "MODE $botnick +i-ws" }
set default-port            6667
set strict-servernames      0
set msg-rate                2
set server-cycle-wait       20
set never-give-up           1
set server-timeout          60
set servlimit               0
set check-stoned            1
set serverror-quit          1
set max-queue-msg           300
set use-console-r           0
set debug-output            0
set raw-log                 0
set quiet-reject            1
set flood-msg               30:60
set flood-ctcp              3:60
set answer-ctcp             3
set lowercase-ctcp          0
set trigger-on-ignore       0
set exclusive-binds         0
set double-mode             0
set double-server           0
set double-help             0
set use-penalties           1
if {${net-type} == 5} {
    set optimize-kicks      1
}
# set check-mode-r          1
set nick-len                30; # Default 9
