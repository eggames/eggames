#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
namespace eval dict {

namespace export check makeRndWord

## Returns if the given word exists in the dictionary
proc check {word} {
	variable dirLang

	set word   [string tolower $word]
	set letter [string index $word 0]
	regsub -all " |.txt" \
		[glob -nocomplain -tails -directory $dirLang *.txt] "" letters

	if {![string match -nocase "*$letter*" $letters]} {return 0}

	set filePath "$dirLang/$letter.txt"
	if {![file exist $filePath]} {return 0}

	set fileHandle [open $filePath r]
	set lstDict [split [read -nonewline $fileHandle] "\n"]
	close $fileHandle

	if {[lsearch $lstDict $word] > -1} {return 1} {return 0}
}
## Creates a random word from dictionary
proc makeRndWord {} {
	variable dirLang

	regsub -all " |.txt" [lsort -dictionary \
		[glob -nocomplain -tails -directory $dirLang *.txt]] "" letters

	set letter [string index $letters [rand [string length $letters]]]
	set filePath "$dirLang/$letter.txt"

	if {![file exist $filePath]} {
		putlog "\/!\\ dictionary (makeRndWord): File $filePath not found."
		return ""
	}
	set fileHandle [open $filePath r]
	set lstWords   [split [read -nonewline $fileHandle] "\n"]
	close $fileHandle

	return [lindex $lstWords [rand [llength $lstWords]]]
}
proc _loadConfig {fileConfPath} {
	variable lang;      variable cmdFlag;  variable cmdAdd
	variable cmdDelete; variable cmdCheck; variable cmdSpeak

	if {![file exist $fileConfPath]} {
		putlog "\(i\) dictionary: File $fileConfPath not found, saving defaults."

		set lang      "en"
		set cmdFlag   "o|o"
		set cmdAdd    "!add"
		set cmdDelete "!delete"
		set cmdCheck  "!check"
		set cmdSpeak  "!speak"

		set f [ open $fileConfPath w ]
		puts $f "variable lang;      variable cmdFlag;  variable cmdAdd"
		puts $f "variable cmdDelete; variable cmdCheck; variable cmdSpeak"
		puts $f ""
		puts $f "set lang      \"$lang\""
		puts $f "set cmdFlag   \"$cmdFlag\""
		puts $f "set cmdAdd    \"$cmdAdd\""
		puts $f "set cmdDelete \"$cmdDelete\""
		puts $f "set cmdCheck  \"$cmdCheck\""
		puts $f "set cmdSpeak  \"$cmdSpeak\""
		close $f
	}
	source $fileConfPath
}
proc _loadLang {} {
	variable dirMain; variable lang

	set fileLang "$dirMain/locale/dict.$lang.tcl"
	if {![file exists $fileLang]} {
		putlog "\/!\\ dictionary (_loadLang): file $fileLang not found."
		return 0
	}
	source $fileLang
	return 1
}
proc _onAdd { nick mask hand chan txt } {
	variable dirLang

	if {![channel get $chan dictionary]} {return}

	set word   [lindex $txt 0]
	set letter [string index $word 0]
	regsub -all " |.txt" \
		[glob -nocomplain -tails -directory $dirLang *.txt] "" letters

	if {![string match -nocase "*$letter*" $letters]} {return}
	set filePath "$dirLang/$letter.txt"
	if { ![ file exist $filePath ] } {
		putlog "\/!\\ dictionary (_onAdd): File $filePath not found."
		return
	}
	set fileHandle [open $filePath r+]
	while {![eof $fileHandle]} {
		set _word [gets $fileHandle]
		if {$word == $_word} {
			putlog "\/!\\ dictionary (_onAdd): \($nick\): $word already exists."
			close $fileHandle
			return
		}
	}
	puts $fileHandle $word
	close $fileHandle
	putquick "PRIVMSG $chan :\0030,2 [msg1]\0037,2\002 $word\002\0030,2 [msg2] [msg5] \003"
}
proc _onDelete { nick mask hand chan txt } {
	variable dirLang

	if {![channel get $chan dictionary]} {return}

	set word   [lindex $txt 0]
	set letter [string index $word 0]
	regsub -all " |.txt" \
		[glob -nocomplain -tails -directory $dirLang *.txt] "" letters

	if {![string match -nocase "*$letter*" $letters]} {return}

	set filePath "$dirLang/$letter.txt"
	if { ![ file exist $filePath ] } {
		putlog "\/!\\ dictionary (_onDelete): File $filePath not found."
		return
	}
	set fileHandle [open $filePath r]
	set lstWords   [split [read -nonewline $fileHandle] "\n"]
	set wordIdx [lsearch $lstWords $word]
	close $fileHandle
	if { $wordIdx < 0 } {
		putlog "\/!\\ dictionary (_onDelete): $word doesn't exists."
		return
	} else {
		set lstWords [ lreplace $lstWords $wordIdx $wordIdx ]
		file delete $filePath
		set fileHandle [ open $filePath w ]
		foreach _word $lstWords {
			puts $fileHandle $_word
		}
		close $fileHandle
		putquick "PRIVMSG $chan :\0030,2 [msg1]\0037,2\002 $word\002\0030,2 [msg2] [msg6] \003"
	}
}
proc _onCheck {nick mask hand chan txt} {

	if {![channel get $chan dictionary]} {return}
	set word [lindex $txt 0]
	if {[check $word]} {set isok [msg2]} {set isok [msg3]}
	putquick "PRIVMSG $chan :\00300,02 [msg1]\00307,02\002 $word\002\00300,02 $isok [msg4] \003"
}
proc _onLangChange {nick uhost hand chan txt} {
	variable lang; variable dirMain; variable dirLang; variable fileLang
	variable cmdSpeak

	set newlang [ lindex $txt 1 ]
	if {![channel get $chan dictionary] || ($newlang == $lang) || \
		([lindex $txt 0] != $cmdSpeak) } { return }

	switch [lindex $txt 1] {
		"en" { set tlang "en" ; putquick "PRIVMSG $chan :Language changed in english" }
		"it" { set tlang "it" ; putquick "PRIVMSG $chan :La lingua corrente e' italiano" }
		"fr" { set tlang "fr" ; putquick "PRIVMSG $chan :Change' de langue en francaise" }
		default { return }
	}
	set filelang "$dirMain/locale/dict.$tlang.tcl"
	if { [ file exists $filelang ] } {
		set lang     $tlang
		set dirLang  "$dirMain/$lang"
		_loadLang
		return
	}
	putlog "\(i\) dictionary (_onLangChange): File $filelang not found."
}
proc _setup {} {
	variable dirMain;  variable cmdFlag; variable cmdAdd; variable cmdDelete
	variable cmdCheck; variable dirLang; variable lang

	set path     [file normalize [info script]]
	set dirMain  [file dirname $path]
	set fileConf "$dirMain/dictionary.conf.tcl"

	_loadConfig $fileConf
	if {![_loadLang]} {return}
	setudef flag dictionary

	set dirLang "$dirMain/$lang"

	bind pubm $cmdFlag *          [namespace current]::_onLangChange
	bind pub  $cmdFlag $cmdAdd    [namespace current]::_onAdd
	bind pub  $cmdFlag $cmdDelete [namespace current]::_onDelete
	bind pub  -        $cmdCheck  [namespace current]::_onCheck

	putlog "Script: dictionary 1.0"
}
_setup

}; # namespace dict
