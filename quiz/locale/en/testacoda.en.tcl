namespace eval testacoda {

proc msgLogo {} {
	return "\0039,1 ~~~~~\0038\002 T\002\00311esta\0038\002C\002\00311oda \0037TCL\0039 ~~~~~ \003"
}
proc msgEndGame {} {
	return "\0039,1 ~~~~~\0038\002 T\002\00311esta\0038\002C\002\00311oda \0037TCL\0039 Over ~~~~~ \003"
}
proc msgURL {} {
	return "\0039,1 ~~~~~\0038 http://eggames.sourceforge.net/\0039 ~~~~~ \003"
}
proc msgHelp {} {
	return "\0039,1 Type in channel a word that starts with the ending part from previous one, using proposed letters. \003"
}
proc msgHelp2 {} {
	set sylLen12 "\0039\(\0038\0022\002\0039\)"
	set sylLen3  "\0039\(\0038\0023\002\0039\)"
	return "\0039,1 E.g. pip\0038po\0039 $sylLen12 --> \0038po\0039llo - pol\0038lo\0039 $sylLen12 --> \0038lo\0039ntra - lon\0038tra\0039 $sylLen3 --> \0038tra\0039ve \003"
}
proc msgHelp3 {} {
	return "\0039,1 Every player will play in a random order and used words will not be accepted. \003"
}
proc msgSubscrAd {} {
	variable timeGap
	return "\0038,2 Subscriptions will starts in\002 $timeGap\002 seconds \003"
}
proc msgSubscr {} {
	variable timeSubscr
	return "\0038,2 You have\0030,2\002 $timeSubscr\002\0038 seconds to subscribe using the command\0030,2\002 $::quiz::cmdSubscr \002\003"
}
proc msgPlayJolly {} {
	variable plrCurrent
	return "\00311,1\002 $plrCurrent\002\0038 plays Jolly and continue the game! \003"
}
proc msgWordPossible {} {
	variable wordPossible
	return "A possible answer: $wordPossible"
}
proc msgWordOk {guess} {
	return "\0039,1 Accepted word:\0037 $guess \003"
}
proc msgSubscribed {who} {
	return "\00311,1\002 $who\002\0039 Subscribed \003"
}
proc msgMancheOne {} {
	variable played; variable word
	return "\0039,1 Let's go!\002 $played\002 total matches played. First word is:\0037 $word \003"
}
proc msgEndSubscr {} {
	return "\0038,2 End of subscriptions! \003"
}
proc msgSubscrWho {} {
	variable playerList
	return "\0039,1 Players are:\00311 $playerList \003"
}
proc msgWait10Secs {} {
	return "\0038,2 In\002 10\002 seconds will starts the game. \003"
}
proc msgSubscrNoMin {} {
	return "\0038,2 Not enought players. \003"
}
proc msgSubscrNone {} {
	return "\0038,2 No players for this round. \003"
}
proc msgPlrTurn {} {
	variable plrCurrent
	return "\0039,1 Player turn:\00311\002 $plrCurrent \002\003"
}
proc msgWordPlay {} {
	variable letters; variable word; variable wordLen; variable wordLenMin
	return "\0039,1 The word is\0037 [string toupper $word]\0038\002 [string length $letters]\002\0039 \(\0038[string toupper $letters]\0039\) - Minimum word length:\0038\002 $wordLenMin\002\0039 - Maximum word length:\0038\002 $wordLen \003"
}
proc msgPlrTime {secs msg} {
	return "\0038,2 You have $secs\002 $msg\003"
}
proc msgPlrTime2 {} {
	return "seconds to type your word. "
}
proc msgPlrTime3 {} {
	variable cmdJolly
	return "To use the Jolly:\0030,2\002 $cmdJolly \002"
}
proc msgTimeUp {} {
	return "\0038,2 Time's up! \003"
}
proc msgPlrOut {} {
	variable plrCurrent
	return "\00311,1 $plrCurrent\0039\002 you loose! \002\003"
}
proc msgWinner {winner} {
	variable manche
	return "\0039,1 After\0038 $manche round(s)\0039 the winner is\00311\002 $winner\002\0039! \003"
}
proc msgScoreTitle {} {
	return "\0037,1 ©º°¨\0039 Hall of Fame\0037 ¨°º© \003"
}
proc msgScoreRes {pos who won} {
	variable gamesWon
	return "\0037,1 $pos\°:\0038 $who\0039 with\0038 $gamesWon($who)\0039 $won \003"
}
proc msgWinnings {num} {
	if {$num > 1} {return "win"} {return "winnings"}
}

}; # namespace testacoda
