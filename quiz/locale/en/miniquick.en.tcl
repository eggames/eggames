namespace eval miniquick {

proc msgLogo {} {
	return "\00315,2 ~~~ \002\00311Mi\00315Ni\00311\00315Q\00311u\00315i\00311C\00315K TCL\002 ~~~ \003"
}
proc msgScoreMax {score} {
	return "\0030,5 Target score:\002 $score \002\003"
}
proc msgHelp {} {
	return "\0030,2 Find a word starting with the top row, you can use a letter for each row. \003"
}
proc msgEndGame {} {
	return "\00315,2 ~~~ \002\00311Mi\00315Ni\00311\00315Q\00311u\00315i\00311C\00315K TCL\002 over! ~~~ \003"
}
proc msgURL {} {
	return "\00315,5 ~\00311\002 http://eggames.altervista.org\002\00315 ~ \003"
}
proc msgTarget {} {
	variable nScoreMax
	return "\00311,2 Target:\0032,11 $nScoreMax \003"
}
proc msgValues {} {
	return "\00311,2 Values:\0032,11 a=1 b=4 c=5 d=4 e=1 f=4 g=5 h=7 i=1 j=8 k=8 l=3 m=3 n=3 o=1 p=4 q=7 r=2 s=2 t=2 u=4 v=5 w=8 x=8 y=8 z=6 \003"
}
proc msgWait5Secs {} {
	return "\0030,5 Next round in 5 seconds \003"
}
proc msgManche {manche} {
	return "\0039,1 MiNi Quick ... manche n°:\002 $manche \002\003"
}
proc msgGridLine {letters} {
	return "\0038,2$letters \003"
}
proc msgGridLineVal {value} {
	return "0,2\002 value \0034,2x $value \002\003"
}
proc msgGridLineMin {} {
	return " \0037,2\002 <-- minimum length \002\003"
}
proc msgQueryBot {botnick} {
	return "\0031,7 Write your word in a PM to\002 $botnick \002\003"
}
proc msgCanRewrite {} {
	return "\00311,1 To change it, you can rewrite it. \003"
}
proc msgTimeAns {} {
	return "\00311,1 You have\0038\002 80\002\00311,1 seconds starting from now. \003"
}
proc msgWarn10Secs {} {
	return "\00311,1 Mancano solo\0034\002 10 \002\00311,1seconds... hurry! \003"
}
proc msgMancheEnd {manche} {
	return "\00311,1 Round\0039\002 $manche \002\00311is over. \003"
}
proc msgPlrEndTime {} {
	return "\0034\002 TIME'S UP! \002\003"
}
proc msgWinner {winner} {
	return "\0039,2 The winner is\0038,2 $winner ! \003"
}
proc msgWinners {winner} {
	return "\0039,2 The winners are\0038,2 $winner ! \003"
}
proc msgResults {} {
	return "\00311,1 Results : \003"
}
proc msgNoWords {} {
	return "\0030,5 No words found! \003"
}
proc msgBonusSpd {} {
	return "\(2 bonus SPEED\)"
}
proc msgBonusLen {} {
	return "\(4 bonus LENGTH\)"
}
proc msgPoints {} {
	return "points \003"
}
proc msgPointsTot {points} {
	return " total\0034,2 $points\0038,2 points \003"
}
proc msgPointsRes {player word score msg} {
	return "\0034,2 $player\0038,2 with the word\0034,2 $word\0038,2 gains\0034,2 $score\0038,2 $msg"
}
proc msgScore {num} {
	return "\0039,2 SCORES after\0038,2 $num\0039,2 rounds... \003"
}
proc msgScoreRes {pos player score} {
	return "\0039,2 $pos .\0034,2 $player\0039,2:\0034,2 $score \0039,2points\003"
}
proc msgWordLenMin {len} {
	return "\0038,4 Minimum word length is about $len letters !\003"
}
proc msgWordInvalid {word} {
	return "\00315 The word\0034\002 $word\002\00315 wasn't found in the grid. Retry ! \003"
}
proc msgWordOk {word score} {
	return "\0039,2 The word\0038,2 $word\0039,2 gives you\0038,2 $score \0039,2points. \003"
}
proc msgWordUnk {} {
	return "\0039,2 That word isn't in my dictionary. \003"
}

}; # namespace miniquick
