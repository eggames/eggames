namespace eval parolazza {

proc msgLogo {} {
	return "\00311,1 Parolazza \0034T\0038C\00311L\0038 Guess the hidden word by following the clues. \003"
}
proc msgEndGame {} {
	variable word
	return "\00311,1 Parolazza \0034T\0038C\00311L\0034 Over!\0038 The hidden word was:\0039 $word \003"
}
proc msgWinner {nick} {
	variable attempts; variable word
	return "\00311,1 Congratulations! You guessed it!\0034\002 $nick\002\00311 after $attempts attempts! The word was:\0039\002 $word\002 \003"
}
proc msgMancheOne {} {
	variable first; variable last
	return "\0038,1 Between\00311 $first\0038 and\00311 $last\0039 \(type your word in channel\) \003"
}
proc msgWinnings {num} {
	if {$num < 2} {return "win"} {return "wins"}
}
proc msgManche {} {
	variable first; variable last; variable attempts
	return "\0038,1 Between\00311 $first\0038 and\00311 $last\0038 \($attempts\)\0037 $::quiz::cmdHint \003"
}
proc msgScoreTitle {} {
	return "\0035,1 ¤¤¤\00311 Top 10\0035 ¤¤¤ \003"
}
proc msgHintWord {} {
	variable first; variable last; variable attempts; variable hintWord
	return "\0038,1 Between\00311 $first\0038 and\00311 $last\0038 \($attempts\)\0037 $::quiz::cmdHint $hintWord \003"
}
proc msgScore {pos entrant won} {
	variable gamesWon
	return "\0037,1 $pos\°:\0038 $entrant\0039 with\0038 $gamesWon($entrant)\0039 $won \003"
}
proc msgDictNotfound {} {return "Dictionary not found in"}
proc msgLoadErr {} {return "- Load error."}

}; # namespace parolazza
