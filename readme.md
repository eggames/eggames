## Main bot files ##

Start bot from terminal:
```
./eggdrop -m demo.conf
```
On IRC:
Query the bot with:
```
/msg <botnick> hello
```
Set password:
```
/msg <botnick> <password>
```
Disconnect bot:
```
/msg <botnick> die <password>
```
Edit demo.conf replacing:
```tcl
bind msg - hello *msg:hello
```
with:
```tcl
unbind msg - hello *msg:hello
```
and save.

From now on start the bot with:
```
./eggdrop demo.conf
```
