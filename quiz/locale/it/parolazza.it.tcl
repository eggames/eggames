namespace eval parolazza {

proc msgLogo {} {
	return "\00311,1 Parolazza \0034T\0038C\00311L\0038 Indovina la parola nascosta seguendo gli indizi. \003"
}
proc msgGameOver {} {
	variable word
	return "\00311,1 Parolazza \0034T\0038C\00311L\0034 Fermato!\0038 La parola era:\0039 $word \003"
}
proc msgURL {} {
	return "\00311,1 http://eggames.sourceforge.net \003"
}
proc msgRoundOne {} {
	variable first; variable last
	return "\0038,1 Tra\00311 $first\0038 e\00311 $last\0039 \(scrivi nel canale per rispondere\) \003"
}
proc msgRound {} {
	variable first; variable last; variable attempts
	return "\0038,1 Tra\00311 $first\0038 e\00311 $last\0038 \($attempts\)\0037 $::quiz::cmdPrefix$::quiz::cmdHint \003"
}
proc msgHintWord {} {
	variable first; variable last; variable attempts; variable hintWord
	return "\0038,1 Tra\00311 $first\0038 e\00311 $last\0038 \($attempts\)\0037 $::quiz::cmdPrefix$::quiz::cmdHint $hintWord \003"
}
proc msgScoreTitle {} {
	return "\0035,1 ¤¤¤\00311 I più matti di Parolazza\0035 ¤¤¤ \003"
}
proc msgScore {pos plr scor} {
	if {$scor > 1} {set msg "vittorie"} {set msg "vittoria"}
	return "\0037,1 $pos\°:\0038 $plr\0039 con\0038 $scor\0039 $msg \003"
}
proc msgWinner {winner} {
	variable attempts
	if {$attempts > 1} {set msg "tentativi"} {set msg "tentativo"}
	return "\00311,1 BrAvOoOoOoOoO\0034\002 $winner\002\00311! Hai indovinato dopo $attempts $msg! \003"
}
proc msgDictNotfound {} {return "Dizionario non trovato in"}
proc msgLoadErr {} {return "- Errore Caricamento."}

}; # namespace parolazza
