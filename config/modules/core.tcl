# Logs
#
set max-logs                5
set max-logsize             0
set quick-logs              0
set log-time                1
set timestamp-format        "\[%H:%M:%S\]"
set quiet-save              1
set keep-all-logs           0
set logfile-suffix          ".%d%b%Y"
set switch-logfiles-at      300
logfile mcwx *              "logs/${botnet-nick}.log"
logfile jk *                "logs/channel.log"
logfile so *                "logs/server.log"
logfile b *                 "logs/bots.log"

# Console
#
set console                 "mkcobxs"

# Files and directories
set sort-users              0
set userfile                "${nick}.user"
set pidfile                 "${nick}.pid"
set help-path               "help/"
set mod-path                "modules/"
set text-path               "text/"
set temp-path               "temp/"
set motd                    "text/motd"
set telnet-banner           "text/banner"
set userfile-perm           0600

# Botnet
#
set remote-boots            2
set share-unlinks           1
set dupwait-timeout         5

# Network
#
set protect-telnet          1
set dcc-sanitycheck         0
set ident-timeout           30
set require-p               1
set open-telnets            0
set stealth-telnets         0
set use-telnet-banner       0
set connect-timeout         1300
set dcc-flood-thr           10
set telnet-flood            5:60
set paranoid-telnet-flood   1
set resolve-timeout         120

# Advanced
#
set ignore-time             15
set hourly-updates          00
set notify-newusers         "$owner"
set default-flags           ""
set must-be-owner           2
set max-dcc                 50
#set whois-fields           "url birthday"
set die-on-sighup           1
set die-on-sigterm          1
set max-socks               100
set allow-dk-cmds           1
set strict-host             1
set cidr-support            0
